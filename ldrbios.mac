;******************************************************************************
;*  L D R B I O S  *  C P M S Y S 3  *  T h o m a s   H o l t e * 8 5 1 1 2 0 *
;******************************************************************************
;*									      *
;*		 M I N I M U M   B I O S   F O R   C P M L D R		      *
;*		 =============================================		      *
;*									      *
;*									      *
;*  Thomas Holte						 Version 1.0  *
;*									      *
;******************************************************************************

	.Z80

;ASCII control codes:
BEL	EQU  07H		;bell
LF	EQU  0AH		;line feed
CR	EQU  0DH		;carriage return
SUB	EQU  1AH		;substitute
ESC	EQU  1BH		;escape
RS	EQU  1EH		;record separator

$SYS1	EQU  0FAH		;system byte 1

;BIOS jump vector.

;All BIOS routines are invoked by calling these entry points.

?BOOT:	JP   BOOT		;initial entry on cold start
	JP   DUMMY		;not used jumps in LDRBIOS
	JP   DUMMY
	JP   DUMMY
?CONO:	JP   CONOUT		;send console output character
	JP   DUMMY		;not used jumps in LDRBIOS
	JP   DUMMY
	JP   DUMMY
?HOME:	JP   HOME		;set disks to logical home
?SLDSK:	JP   SELDSK		;select disk drive, return disk parameter info
?STTRK:	JP   SETTRK		;set disk track
?STSEC:	JP   SETSEC		;set disk sector
?STDMA:	JP   SETDMA		;set disk I/O memory address
?READ:	JP   READ		;read physical block(s)
	JP   DUMMY		;not used jumps in LDRBIOS
	JP   DUMMY
?SCTRN:	JP   SECTRN		;translate logical to physical sector
	JP   DUMMY		;not used jumps in LDRBIOS
	JP   DUMMY
	JP   DUMMY
	JP   DUMMY
	JP   DUMMY
	JP   DUMMY
	JP   DUMMY
	JP   DUMMY
?MOVE:	JP   MOVE		;block move memory to memory
	JP   DUMMY		;not used jumps in LDRBIOS
	JP   DUMMY
	JP   DUMMY
	JP   DUMMY


*EJECT
BOOT:
;====

;Initial entry point for system startup.

	JP   ?INIT		;perform any additional system initialization
				;and print signon message


CONOUT:
;======

;Console output. Send character in reg. C to all selected devices.
	LD   A,C		;character  --> accu
	LD   C,4		;function # --> reg. C
	JP   ?USERF		;display char


HOME:
;====

;Home selected drive. Treated as SETTRK (0).

	LD   BC,0		;same as set track zero
	JR   SETTRK


SELDSK:
;======

;Select disk drive. Drive code in reg. C. Invoke login procedure for drive if
;this is first select. Return address of disk parameter header in reg. HL.

	LD   HL,FD0		;^extended disk parameter header --> reg. HL
	RET

;Extended Disk Parameter Header (XDPH)
	DEFS 8
	DEFB 0,0		;relative drive zero
FD0:	DEFW 0			;no translation table
	DEFW 0,0,0,0		;BDOS scratch area
	DEFB 0,0		;media flag
	DEFW DPB0		;disk parameter block
	DEFW CSV0		;checksum vector
	DEFW ALV0		;allocation vector
	DEFW DIRBCB		;DIRBCB alloc'd by GENCPM
	DEFW DTABCB
	DEFW 0FFFFH		;no HASH
	DEFB 0			;hash bank

;Disk Parameter Block (DPB)
DPB0:	DEFW 80			;128 byte records per track
	DEFB 4,15		;block shift and mask
	DEFB 0			;extent mask
	DEFW 389		;maximum block number
	DEFW 191		;maximum directory entry number
	DEFB 0E0H,0		;alloc vector for directory
	DEFW 48			;checksum size
	DEFW 2			;offset for system tracks
	DEFB 2,3		;physical sector size shift and mask

;directory buffer control block:
DIRBCB:	DEFB 0FFH		;drive
	DEFS 4			;record #, written ?
	DEFB 0
	DEFS 4			;track, sector
	DEFW DIR		;buffer address

;data buffer:
DTABCB:	DEFB 0FFH		;drive
	DEFS 4			;record #, written ?
	DEFB 0
	DEFS 4			;track, sector
	DEFW DATA		;buffer address


SETTRK:
;======

;Set track. Saves track address from reg. BC in @TRK for further operations.

	LD   (@TRK),BC
	RET


SETSEC:
;======

;Set sector. Saves sector number from reg. BC in @SECT for further operations.

	LD   (@SECT),BC
	RET


SETDMA:
;======

;Set disk memory address. Saves DMA address from reg. BC in @DMA.

	LD   (@DMA),BC
	RET


READ:
;====

;Read physical record from currently selected drive. Finds address of proper
;read routine from extended disk parameter header (XDPH).

FD$READ:LD   A,(@SECT)		;sector number	       --> reg. B
	LD   B,A
	LD   A,(@TRK)		;track number	       --> reg. E
	LD   E,A
	XOR  A			;absolute drive number --> accu
	LD   HL,(@DMA)		;user buffer address   --> reg. HL
	LD   C,11		;function number       --> reg. C
	CALL ?USERF		;read physical disk sector
	OR   A			;any errors ?
	RET  Z
	LD   A,1		;common error code
	RET


SECTRN:
;======

;Sector translate. Indexes skew table in reg. DE with sector in reg. BC.
;Returns physical sector in reg. HL. If no skew table (reg. DE = 0) then
;returns physical = logical.
	LD   L,C
	LD   H,B
	RET


MOVE:
;====
	EX   DE,HL		;we are passed source in DE and dest in HL
	LDIR			;use Z80 block move instruction
	EX   DE,HL		;need next addresses in same regs
DUMMY:	RET


?INIT:	DI			;disable interrupts
	IM   1			;interrupt mode 1 (RST 7)
	LD   C,23		;function # --> reg. C
	CALL ?USERF		;general system initialization
	RET

*EJECT
;table of driver entry vectors:
$VDINIT	EQU  0			;initialize the video controller chip M6845
$RSINIT	EQU  1			;initialize the RS-232-C interface
$KBCHAR	EQU  2			;get a keyboard character if available
$KBWAIT	EQU  3			;wait for a keyboard character
$VDCHAR	EQU  4			;display a character
$PRSTAT	EQU  5			;test printer status
$PRCHAR	EQU  6			;output a character to the printer
$RSRCST	EQU  7			;get a character from the RS-232-C interface if
				;available
$RSRCV	EQU  8			;receive a character from the RS-232-C
				;interface
$RSTXST	EQU  9			;test the RS-232-C output status
$RSTX	EQU  10			;transmit a character to the RS-232-C interface
$READ	EQU  11			;read a disk sector
$WRITE	EQU  12			;write a disk sector
$GETTIM	EQU  13			;get time and date in binary format
$SETTIM	EQU  14			;set time and date in binary format
$XMOVE	EQU  15			;interbank data transfer
$GTIME3	EQU  18			;get time and date in CP/M 3 format
$STIME3	EQU  19			;set time and date in CP/M 3 format
$LDCHAR	EQU  20			;load bit pattern into character RAM
$RSTCHR	EQU  21			;restore original character set
$SCRNIO	EQU  22			;direct screen I/O
$INIT	EQU  23			;general system initialization
$CLOCK	EQU  24			;display tim continuously
$DISP	EQU  25			;turn on/off graphic display
$CLS	EQU  26			;clear graphic screen
$PLOT	EQU  27			;plot dot    on graphic screen
$POINT	EQU  28			;read dot  from graphic screen
$LINE	EQU  29			;draw line   on graphic screen
$CIRCLE	EQU  30			;draw circle on graphic screen
$ARC	EQU  31			;draw arc    on graphic screen
$COPY	EQU  33			;copy area


;call ROM driver routines:
?USERF:	LD   (SAVESP),SP	;save stack pointer
	LD   SP,0E100H		;temporary stack --> stack pointer
	PUSH BC			;save register set
	PUSH DE
	PUSH HL
	PUSH IX
	PUSH IY
	DI			;disable interrupts
	RST  0			;call driver routine
	PUSH AF			;save accu
	LD   A,11010101B	;disable memory mapped I/O
				;disable graphic display
				;disable boot EPROM
				;disable graphic page
				;7.2 MHz clock frequency
				;enable video display
	OUT  ($SYS1),A		;write system byte 1
	POP  AF			;restore accu
	POP  IY			;restore register set
	POP  IX
	POP  HL
	POP  DE
	POP  BC
	LD   SP,(SAVESP)	;restore stack pointer
	RET			;return to caller

SAVESP: DEFS 2			;temporary memory for stack pointer


*EJECT
@TRK:	DEFS 2			;current track number
@SECT:	DEFS 2			;current sector number
@DMA:	DEFS 2			;current DMA address


CSV0	EQU  $
ALV0	EQU  CSV0+48
DIR	EQU  ALV0+49
DATA	EQU  DIR+512

	END


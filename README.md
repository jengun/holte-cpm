In 1984/1985, Thomas Holte wrote versions of CP/M 2.2 and 3.0 (Plus) for the
Z80 microcomputer systems EACA EG 3200 [Genie III] and the TCS [Genie IIIs].

Thanks to the efforts of Fritz Chwolka in 1990, Thomas Holte kindly released
the complete source code of these CP/M versions, under the conditions that

  * His name as the original author is always retained
  * Commercial use and any liability or warranty are excluded

Source files in this repository are under the [CC BY-NC-SA license](LICENSE).

[Genie III]:  https://oldcomputers-ddns.org/public/pub/rechner/eaca/genie_3/
[Genie IIIs]: https://oldcomputers-ddns.org/public/pub/rechner/eaca/genie_3s/

.PL 68
.LH 12
.HEGenie IIIs CP/M Version 3b                               Seite #
.FO                                        (C) 1985 by Thomas Holte
1 ��Mini Disk Operation

����CP/͠ erforder� wenigsten� ein� Diskettenlaufwer� un� kan� bi� �
����z� acht� Laufwerk� ansteuer� (� - H)� Da� CP/M-System� welche� �
����Si� erhalte� haben� is� f}� di� Steuerun� zweie� doppelseiti�
����ge� 5-Zoll-Minidisklaufwerk� mi� jeweil� 8� Spure� konfigu�
����riert� Dies� Laufwerk� sin� normalerweis� i� Ihre� Geni� III� �
����eingebaut�� M|chte� Si� ei� bi� sech� weiter� Laufwerk� (5� �
����ode� 8"� a� Ihre� Geni� anschlie~en�� k|nne� Si� gege� ein� �
����Unkostenpauschal� vo� D� 100,- (zzgl� MWSt� ei� speziel� kon�
����figurierte� Syste� erhalten�� Bitt� wende� Si� sic� a� nach�
����stehend� Adresse:

����                Thomas Holte
����                Sommerstr.16
����           8504 Stein

����Systemkundig� k|nne� mi� Hilf� de� mitgelieferte� BIOS-Quell�
����programm� selbs�� ei� entsprechende� Syste�� konfigurieren� �
����wen� si� i� Besit� de� Assembler� MACRO-8�� vo� Microsof� �
����sind.

����Wen� de� Rechne� ein� Urladeoperatio� bei� Einschalte� bzw� �
����"Reset�� durchf}hrt�� versuch� e� automatisch�� da� Betriebs�
����syste�� vo� Laufwer� � z� laden�� Deshal� mu� sic� ein� CP/� �
����Systemdiskett� bei� Einschalte� bzw�� "Reset� de� Rechner� i� �
����Laufwer� (� bzw� A� befinden.


1.1 Funktionsweise

����Bevo� Si� de� Rechne� einschalten�� sollte� Si� einig� Fakte� �
����}be� di� Funktionsweis� Ihre� Diskettenlaufwerk� kennenler�
����nen.

����Da� Laufwer� rotier� nich� ununterbrochen�� w{hren� e� ein�
����geschalte� ist�� sonder� nur�� wen� ei� "MOTO� ON"-Signa� vo� �
����Rechne� gesende�� wird�� Wen� meh� al� ei� Laufwer� a� de� ��������Rechne� angeschlosse� ist�� schalte� da� "MOTO� ON"-Signa� �
������all� Motore� ei� bzw�� aus�� auc� wen� de� Rechne� nu� au� �
������ein� Laufwer� zugreift� Diese� Signa� wir� zirk� ein� Sekun�
������de�� bevo� de� Rechne� au� ei� Laufwer� zugreift� gesendet� �
������dami�� di� Laufwerk� di� notwendig� Umdrehungsgeschwindig�
������kei� erreiche� k|nnen.

������W{hren� de� Rechne� au� ei� Laufwer� zugreift� leuchte� di� �
������rot� Diod� (LED� au� de� Frontseit� de� jeweilige� Lauf�
������werks.
������Achtung: \ffne� Si� nich� di� Laufwerksklappe�� w{hren� di� �
���������������Diod� leuchtet.


1.1.1 Funktionsweise der Diskette

      Ein� Diskett� is� ein� einfach� Plastikscheibe� dere� Ober�
������fl{ch� ferromagnetisc� beschichte� un� polier� ist� [hnlic� �
������eine� Single-Schallplatt� besitz� si� ei� gro~e� Spindel�
������loch� u� sic� de� Laufwerksnab� anzupassen� un� ei� kleine� �
������Indexloch� da� vo� Laufwer� registrier� wird� wen� di� Dis�
������kett� rotiert.

������Ei� leer� Diskett� (ne� ode� magnetisc� gel|scht�� enth{l� �
������kein� Information�� Au� Ihre� CP/M-Systemdiskett� befinde� �
������sic� deshal� ei� spezielle� Dienstprogram�� (FORMAT)�� da� �
������ein� Diskett� i� "Spuren� un� "Sektoren� einteilt� (Weiter� �
������Informatione� sieh� Kap� 2.1� Dienstprogramme� FORMAT).

������Jed� Diskett� befinde� sic� andauern� i� eine� H}lle�� wel�
������ch� ihr� Oberfl{ch� vo� Verkratze� ode� Ber}hre� sch}tzt� �
������Di� Diskett� rotier� i� Laufwer� i� ihre� Schutzh}lle�� Di� �
������H}ll� besteh� inne� au� eine� spezielle� Material�� welche� �
������di� Diskett� w{hren� de� Rotatio� reinigt.

������Beachte� Si� de� kleine� Aufklebe� a� obere� Ran� Ihre� �
������Systemdiskett� (oberhal� de� Labels)�� Diese� Aufklebe� be�
������deck�� di� Schreibschutzkerb� de� Diskette�� Dami� is�� di� ��������Diskett� physisc� vo� eine� Beschreibe� gesch}tzt�� (Unte� �
������eine� "Schreiboperation� versteh� ma� jede� [nder� de� au� �
������de� Diskett� gespeicherte� Daten�� I� Gegensat� daz� ver{n�
������der�� ein� "Leseoperation� dies� Date� nich� - e� wir� le�
������diglic� au� si� zugegriffen)�� Entferne� Si� als� diese� �
������Aufkleber�� wen� Si� ein� Diskett� beschreibe� m|chten� �
������bringe� Si� ih� an�� wen� Si� ein� zuf{llig� Schreibopera�
������tio� verhinder� wollen.


1.1.2 Einf}hren der Diskette in das Laufwerk

������a) Achte� Si� darauf� da� di� rot� Diod� au� de� Frontseit� �
���������de� Laufwerk� nich� leuchtet�� wen� Si� ein� Diskett� i� �
���������da� Laufwer� einf}hre� bzw� ih� entnehmen.

������b) \ffne� Si� di� Laufwerksklappe�� F}hre� Si� di� Diskett� �
���������mi� de� Schreibschutzkerb� nac� link� un� de� Labe� nac� �
���������obe� waagrech� vorsichtig� i� da� Laufwer� ein� Schlie~e� �
���������Si� di� Klapp� nicht� bevo� di� Diskett� gan� eingef}hr� �
���������ist� sons� k|nnt� di� Diskett� besch{dig� werden.

������c) Schlie~e� Si� di� Klappe�� Dami� erfa~� di� Nab� de� �
���������Laufwerk� di� Diskett� i� Spindelloch�� L{~� sic� di� �
���������Klapp� nich� leich� schlie~en�� wende� Si� kein� Gewal� �
���������an�� Nehme� Si� di� Diskett� herau� un� probiere� Si� e� �
���������noc� einmal.


1.1.3 Einschalten des Ger{tes

      Gehe� Si� bei� Einschalte� de� Ger{te� nu� i� nachstehende� �
������Reihenfolg� vor:

������a) F}hre� Si� vorsichti� ein� Diskette�� au� de� sic� da� �
���������CP/M-Syste� befindet� i� da� Laufwer� � ode� � ein� ohne� �
���������di� Klapp� z� schlie~en.
.PA�������b) Schalte� Si� de� Rechne� ein��

������c) Schlie~e� Si� di� Laufwerksklappe�� Da� Betriebssyste� �
���������wir� nun vo� Rechne� geladen.


1.2 Pflege der Disketten

  ����a) Lasse� Si� Diskette� i� de� mitgelieferte� Papiertasche� �
���������solang� si� nich� i� eine� de� Laufwerk� stecken� Lasse� �
���������Si� nich�� unn|tigerweis� Diskette� i� de� Laufwerken� �
���������z.B� wen� da� Syste� abgeschalte� ist.

������b) Halte� Si� Diskette� entfern� vo� magnetische� Felder� �
���������(Transformatoren�� Wechselstrommotoren�� Magnet� usw.)� �
���������Stark� magnetisch� Felde� zerst|re� di� au� de� Diskett� �
���������gespeichert� Information.

������c) Nehme� Si� Diskette� nu� mi� ihre� H}ll� - ber}hre� Si� �
���������kein� ihre� offenliegende� Oberfl{chen�� Versuche� Si� �
���������nicht�� di� Diskettenoberfl{ch� z� reinigen� Si� k|nnte� �
���������si� verkratze� un� zerst|ren.

������d) Halte� Si� Diskette� entfern� vo� Hitz� un� direkte� �
���������Sonneneinstrahlun� (sieh� Kap�� 1.3�� Technisch� Daten� �
���������Lagertemperatur).

������e) Vermeide� Si� ein� Ber}hrun� de� Diskette� mi�� Zigaret�
���������tenasche� Stau� ode� andere� Partikeln.

������f) Beschrifte� Si� da� Diskettenlabe� nich� mi� eine�� Ku�
���������gelschreiber�� d� die� di� Diskettenoberfl{ch� besch{di�
���������ge� k|nnte� Benutze� Si� ausschlie~lic� Filzstift.

������g) Vergewisser� Si� sich�� da�� di� Diod� (LED�� au� de� �
���������Frontseit� de� Laufwerk� nich� leuchtet�� wen� Si� ein� �
���������Diskett� einf}hren.
�������h) Lager� Si� Diskette� senkrech� ({hnlic� Schallplatten)� �
���������dami� si� vo� seitliche� Druc� gesch}tz� werden.


1.2.1 Sie haben Probleme ...

������Wiederhol� auftretend� Schreib-/Lesefehle� w{hren� de� Dis�
������kettenzugriff� de� Rechner� k|nne� durc� besch{digt� Dis�
������ketten�� Laufwerk� ode� ander� Hardwar� verursach�� werden� �
������Versuche� Sie� de� Fehle� z� isolieren� inde� Si� Laufwerk� �
������un� Diskette� wechseln.

������Habe� Si� wiederhol� Problem� mi� eine� bestimmte� Disket�
������te�� versuche� Sie�� di� au� ih� gespeicherte� Dateie� au� �
������ein� ander� Diskett� z� kopieren�� Versuche� Si� dann�� di� �
������fehlerhaft� Diskett� ne� z� formatieren.

������Schein� da� Laufwer� fehlerhaf� z� sei� (wiederhol� auftre�
������tend� Fehle� w{hren� de� Zugriff� au� Diskette� unter�
������schiedliche� Herkunft)�� wende� Si� sic� bitt� a� Ihre� �
������H{ndler.
.PA�1.3 Technische Daten

    Laufwerk:
     
     Speicherkapazit{t : 800 KBytes formatiert
                         774 KBytes f}r den Benutzer verf}gbar
     Speichereinteilung:   2 Oberfl{chen
                          80 Spuren   pro Oberfl{che
                          10 Sektoren pro Spur 
                         512 Bytes    pro Sektor
     Datentransferrate :  25 KBytes pro Sekunde
     Durchschnittliche
     Zugriffszeit      : 250 Millisekunden
 ����Motorstartzeit    :   1 Sekunde

�����Diskette:

�����Lebensdauer    : 2.�� Millione� Umdrehunge� pr� Spu� (11� �
����������������������Stunden)�� entsprich�� ca�� f}n� Jahr� be�       �
����������������������normale� Gebrauch
  ���Lebensdauer der
�����gesp. Daten    : 20 Jahre
�����Lagertemperatur: 10 - 50 Grad Celsius
�����Abmessungen    : 13.3 x 13.3 x 0.08 cm


1.4 Anfertigen einer Sicherungskopie

����Bevo� Si� irgendetwa� andere� mi� Ihre� CP/M-Systemdiskett� �
����machen�� befolge� Si� dies� Anweisungen�� u� mindesten� zwei� �
����Sicherungskopie� Ihre� Originalsystemdiskett� z� machen� W}r�
����de� Si� sic� nich� dara� halten�� w{re� Si� "au� de� Rennen"� �
����wen� Ihre� Originaldiskett� etwa� geschieh� un� Si� au� ein� �
����neu� warten.

����Schalte� Si� Ihre� Rechne� wi� i� Kap� 1.1.� beschriebe� ein�     �
����Bringe� Si� keinen� Schreibschutzaufklebe� au� de� Original�
����diskett� an!�����Nachde� da� Syste� urgelade� hat� nehme� Si� di� Originaldis�
����kett� au� de� untere� Laufwer� un� bringe� eine� Schreib�
����schutzaufklebe� au� ih� an� Lade� Si� dan� erneu� ur!

����F}hren Sie nun eine neue Diskette in das obere Laufwerk ein.

����Geben Sie       FORMAT B <NEW LINE>
����ein.

����Das System wird dann anzeigen:
    M|chten Sie wirklich die Diskette in Laufwerk B formatieren ?

����Antworte� Si� mi� "J"�� Ihr� Diskett� wir� nu� formatiert� �
����Nac� Beendigun� de� Formatiervorgang� zeig�� da� Syste� �
����"Programm beendet� an� Wiederhole� Si� diese� Vorgan� f}� di� �
����zweit� Diskette.

����Nun geben Sie 
����                KOPIER {V} <NEW LINE>
����ein�� Di� eckige� Klammer� werde� i� deutsche� Zeichensat� �
����durc� "[� un� "]� dargestellt�

����Da� Kopierprogram� beginn� nu� eine� Dialo� mi�� Ihnen�� be� �
����de�� Si� folgend� Antworte� gebe� m}sse� (Antworte� fettge�
����druckt):

����Kopier Ver 3.0

    Modus    Funktion

����ALLES    Kopieren der ganzen Diskette
����SYSTEM   Kopieren der Systemspuren
����DATEN    Kopieren der Datenspuren
����ENDE     Programmende

����Modus: ALLES <NEW LINE>
.PA�����Quellaufwerk: A <NEW LINE>

����Ziellaufwerk: B <NEW LINE>

    (^C F}r Programmabbruch)
    Dr}cke� Si� <ENTER>� u� ALLE� vo� � nac� � z� kopiere� <NE� LINE>

����Nac� de� Kopiere� de� erste� Diskett� melde� sic� da� Pro�
����gram� mi� de� Frage
    M|chten Sie das Kopieren wiederholen ?

����Antworte� Si� mi� "J"�� Di� zweit� Sicherungskopi� wir� nu� �
����angefertigt�� Bewahre� Si� da� Origina� un� mindesten� eine �
����Sicherungskopi� a� eine� sichere� Or� auf�� Arbeite� Si� nu� �
����mi� eine� Kopi� Ihre� Originalsystems.

����Achtung: Da� Dienstprogram� KOPIE� dien� au� urheberrechtli�
�������������che� Gr}nde� nu� z� Ihre� pers|nliche� Gebrauch� da�
�������������mi�� Si� Sicherungskopie� Ihre� CP/M-System� un� Ih�
�������������re� Datendiskette� anfertige� k|nnen.
.PA�2 Genie-IIIs-CP/M-Dienstprogramme

����Diese� Kapite� setz� voraus�� da� Si� da� CP/M-Betriebssyste� �
����bereit� kenne� ode� sic� mi� de� "CP/� Plu� User'� Guide� vo� �
����Digita� Researc� vertrau� gemach� haben� Weiterhi� is� darau� �
����hinzuweisen� da� e� sic� be� nachstehen� aufgef}hrte� Dienst�
����programme� u�� rechnerspezifisch� Programm� handelt�� d.h� �
����dies� Programm� sin� nu� au� Ihre� Geni� III� Syste� ablauf�
����f{hig.

����Allgemei� stehe� be� de� men}gesteuerte� Dienstprogramme� �
����nachstehend� Editierfunktionstaste� zu� Verf}gung:

����             Taste        !        Bezeichnung
         =====================+============================
                 Hochpfeil    ! Kursor Zeile hoch
         ---------------------+----------------------------
         SHIFT + Hochpfeil    ! Kursor erste Fensterzeile
         ---------------------+----------------------------
                 Abw{rtspfeil ! Kursor Zeile abw{rts
         ---------------------+----------------------------
         SHIFT + Abw{rtspfeil ! Kursor letzte Fensterzeile
         ---------------------+----------------------------
                 Linkspfeil   ! Kursor Spalte links
         ---------------------+----------------------------
         SHIFT + Linkspfeil   ! Kursor erste Fensterspalte
         ---------------------+----------------------------
                 Rechtspfeil  ! Kursor Spalte rechts 
         ---------------------+----------------------------
         SHIFT + Rechtspfeil  ! Kursor letzte Fensterspalte
         ---------------------+----------------------------
                 CLEAR        ! L|schen bis Zeilenende
         ---------------------+----------------------------
         SHIFT + CLEAR        ! L|schen des Fensters
         ---------------------+----------------------------
         F1                   ! Einf}gemodus
         ---------------------+----------------------------
         F2                   ! Einf}gen Zeile�����             Taste        !        Bezeichnung
         =====================+============================
����     F3                   ! L|schen Zeichen
����     ---------------------+----------------------------
����     F5                   ! L|schen Zeile


2.1�FORMAT:

����Aufruf:
����                FORMAT Laufwerk

����F}� "Laufwerk� m}sse� Si� de� Name� de� logische� CP/M-Lauf�
����werks� au� de� Si� ein� Diskett� formatiere� wollen� angeben.

����Mi� diese� Dienstprogram� k|nne� Si� neu� Diskette� formatie�
����re� bzw�� alt� Diskette� ne� formatieren�� d.h� i� Spure� un� �
����Sektore� einteilen� 

����Beispiel:

����FORMAT B
����M|chten Sie wirklich die Diskette in Laufwerk � formatieren � J

����Mi�� diese�� Kommand� wir� di� i�� rechte� Laufwer� Ihre� �
����GenieII�s befindlich� Diskett� formatiert.


2.2�FTASTEN:

����Aufruf:
����                FTASTEN

����Achtung:� Bevo� Si� FTASTE� aufrufen�� m}sse� Si� de� Schreib�
����schutzaufklebe� Ihre� Systemdiskett� entfernen.

����Mi�� diese� Dienstprogram� k|nne� Si� di� dreizeh� Funktions�
����taste� Ihre� Geni� III� programmieren� d.h� mi� eine� au� ma������xima� 7� Zeiche� bestehende� "String� belegen�� Steuerzeiche� �
����werde� mi� eine� Zirkumfle� un� de�� zugeh|rige� Buchstabe� �
����eingegeben� z.B� "^C� f}� CONTROL-� ode� "^M� f}� <NEWLINE>� �
����Leerzeiche� m}sse� mi� eine� Unterl{ng� eingegebe� werden� �
����z.B�� DIR_B�� FTASTEΠ l{uf�� men}gesteuer� a� un� kan� mi� �
����<BREAK� abgebroche� werden� 


2.3�KONFIG:

����Aufruf:
����                KONFIG

����Achtung: Bevo� Si� KONFI� aufrufen�� m}sse� Si� de� Schreib�
����schutzaufklebe� Ihre� Systemdiskett� entfernen.

����Mi�� diese� Dienstprogram� k|nne� Si� da� CP/M-Betriebssyste� �
����Ihre� Geni� III� innerhal� gewisse� Grenze� Ihre� pers|nli�
����che� W}nsche� bzw�� de� vorhandene� Peripherieger{te� anpas�
����sen�� KONFIǠ l{uf� men}gesteuer� a� un� kan� mi� <BREAK� ab�
����gebroche� werden.

����Achtung: U� di� ge{nderte� Paramete� auc� benutze� z� k|nnen� �
����    ���� m}sse� Si� <RESET� dr}cken.

����Anmerkungen zum Einstellen der Systemparameter:
����Sollt� Ih� Rechne� }be� zus{tzliche� Speiche� (6� ode� 12� �
����KBytes� verf}gen� k|nne� Si� die� hie� angeben� E� steh� dan� �
����ein� ultraschnell� Pseudoflopp� mi� 5� bzw�� 11� KByte� Spei�
����cherkapazit{� zu� Verf}gun� (Laufwer� M).
����E� werde� di� Uhrenkarte� de� Firme� TC� Compute� GmbȠ un� �
����HJ� Computersystem� unterst}tzt� Vorteil� eine� Uhrenkarte:
����- Hohe Ganggenauigkeit
����- Kei� Neueinstelle� vo� Datu� un� Uhrzei�� be� Einschalte� �
������de� Rechners
.PA�����Anmerkungen zum Einstellen der Laufwerksparameter:
����Di� Einstellm|glichkeite� de� Laufwerksparamete� versetze� �
����Si� i� di� Lage� Diskette� fast� alle� au� de� Mark� befindli�
����che� CP/M-Rechne� z� lese� bzw�� z� beschreiben�� Si� m}sse� �
����nu� da� physische� Forma� de� fremde� Diskette� kennen� u� di� �
����entsprechende� Einstellunge� vornehme� z� k|nnen�� Dami�� da� �
����Geni� III� CP/� Forma� bei� Umstelle� de� Laufwerksparamete� �
����nich�� verlorengeht�� wir� ei� logisches� Laufwer� Р konfigu�
����riert�� welche� jede� vorhandene� physischen� Laufwer� au~e� � �
����}berlager� werde� kann.


����Beispiele:

����a) Format des Genie IIIs CP/M Systems 2.2:

�������- doppelseitig
�������- doppelte Dichte
�������- doppelte Dichte der ersten Spur
�������- 80 nutzbare Spuren
�������- 10 Sektoren pro Spur
�������- 1024 Bytes Sektorl{nge
�������- Nummer des ersten Sektors: 0
�������- "Interleaving"-Faktor: 2
����   - Keine Fortsetzung der Sektornumerierung auf der
����     R}ckseite
�������- 4 KBytes Blockgr|~e
�������- 8 KBytes "Directory"-Gr|~e
����   - ]bersetzungsfaktor: 1
�������- 3 Systemspuren

�������Di� Begriff� Blockgr|~e�� "Directory"-Gr|~� un� Systemspu�
�������re� sin� i� "CP/� Plu� Syste� Guide� vo� Digita� Researc� �
�������erl{utert.
.PA�����b) Einstelle�de� Standard CP/M Formate� (IB͠3740):

�������Dies� Einstellun� is� nu� m|glich�� wen� Si� ei� 8-Zoll-�
�������Laufwer� a� Ihre� Geni� III� angeschlosse� haben�� F}� da� �
�������}berlagert� Laufwer� C� D� E� F� � ode� � is� anzugeben:

�������- 8-Zoll Diskettengr|~e
�������- einseitig
�������- einfache Dichte
�������- 77 nutzbare Spuren
�������- 26 Sektoren pro Spur
�������- 128 Bytes Sektorl{nge
�������- Nummer des ersten Sektors: 1
�������- "Interleaving"-Faktor: 1
�������- 1 KByte Blockgr|~e
�������- 2 KBytes "Directory"-Gr|~e
�������- ]bersetzungsfaktor: 6
�������- 2 Systemspuren

����c)�Da� Standardforma� Ihre� neue� CP/M-Systems�� au� da� auc� �
�������Laufwer� � eingestell� ist:

�������- doppelseitig
�������- doppelte Dichte
�������- doppelte Dichte der ersten Spur
�������- 80 nutzbare Spuren
�������- 20 Sektoren pro Spur
�������- 512 Bytes Sektorl{nge
�������- Nummer des ersten Sektors: 0
�������- Kein� Fortsetzun� de� Sektornumerierun� au� de� �
���������R}ckseite
�������- "Interleaving"-Faktor: 2
�������- 2 KBytes Blockgr|~e
�������- 6 KBytes "Directory"-Gr|~e
�������- ]bersetzungsfaktor: 1
�������- 2 Systemspuren
.PA�����Unte� "Interleaving"-Fakto� versteh� ma� de� Abstan� zweie� �
����aufeinanderfolgende� physischer� Sektore� i� Sektoren� D� bei� �
����IB͠ 3740-Forma�� zwe� logisc� aufeinanderfolgend� Sektore� �
����physisc� ebenfall� direk�� hintereinande� liege� ("Inter�
����leaving"-Fakto� 1)� verwende� da� CP/M-Betriebssyste� ei� ei�
����gene� sogenannte� "Soft-Interleaving"�� u� di� Zugriffszeite� �
����z� optimieren�� Ohn� diese� "Interleaving� w{re� 2� Umdrehun�
����ge� n|tig�� u� nu� ein� Spu� eine� Diskett� diese� Format� z� �
����lesen�� Diese� "Soft-Interleaving� wir� bei� Men}punk� "]ber�
����setzungsfaktor�� eingegeben�� Dami�� wir� da� BIO� s� einge�
����stellt�� da�� di� logisch� Sektornumme� vo� de�� Diskzugrif� �
����noc� i� ein� physisch� Sektornumme� umgerechne� wir� (BIOS-�
����Aufru� SECTRAN)� 

����Be� 5-Zoll-Diskette� is� diese� "Soft-Interleaving�� nich� �
����}blich� Hie� werde� di� Sektornummer� gleic� bei� Formatiere� �
����entsprechen� verschr{nk� ("Hard-Interleaving")� Diese� "Hard-�
����Interleaving�� k|nne� Si� mi� KONFI� durc� Angab� eine� ent�
����sprechende� Faktor� beeinflussen�� De� "Interleaving"-Fakto� �
����sollt� s� gew{hl� werden�� da� sic� bei� Diskettenzugrif� op�
����timal� Schreib-/Lesezeite� ergeben.

����I� CP/� Vers� 3�  erstreck� sic� eine� Spu� be� doppelseitige� �
����Diskette� imme� }be� beide� Oberfl{chen�� wobe� au� de� R}ck�
����seit� wiede� be� Nul� mi�� de� Sektornumerierun� begonne� �
����wird.

����De� "Interleaving"-Fakto� mu� nu� dan� angegebe� werden� wen� �
����Si� Diskette� mi� de� ne� eingestellte� Parameter� formatie�
����re� wollen�� Achte� Si� darauf�� da� be� Einstellun� au� da� �
����Standardforma� Ihre� neue� System� au� jede� Fal� de� "Inter�
����leaving"-Fakto� �� gew{hl� wird�� den� nu� s� sin� optimal� �
����Zugriffszeite� gew{hrleistet.
.PA�����Nachstehend� Sektorenanzahle� pr� Spu� un� Oberfl{ch� sin� �
����maxima� m|glich:

����Sektorl{ng� � einfach�      ! einfach�      � doppelt�
���� in Bytes   ! Dichte 8-Zoll ! Dichte 5-Zoll ! Dichte 5-Zoll
����============+===============+===============+================
����     128    !       26      !      16       !      ---
����------------+---------------+---------------+----------------     
����     256    !       17      !      10       !       18
����------------+---------------+---------------+----------------
����     512    !        9      !       5       !       10
����------------+---------------+---------------+---------------- 
����    1024    !      ---      !     ---       !        5


����FORMAԠ kan� di� erste� Spu� vo� Diskette� doppelte� Dichte� �
����dere� erst� Spu� au� einfach� Dicht� eingestell�� ist�� nich� �
����formatieren.

����Sollt� e� nich� m|glic� sein�� mi� KONFI� da� Diskettenforma� �
����eine� Fremdrechner� mi�� CP/M-Betriebssyste�� einzustellen� �
����k|nne� Si� gege� ein� Unkostenpauschal� vo� D͠ 100,- (zzgl� �
����MWSt�� ei� speziel� konfigurierte� Syste� erhalten�� mi�� de� �
����Si� Diskette� diese� Format� lese� k|nnen�� Sende� Si� hierz� �
����ein� neue��� formatierte�� Diskett� de� Fremdrechners�� di� �
����m|glichs� viel� Date� enth{lt� a� nachstehend� Adresse:
����                
����                Thomas Holte
����                Sommerstr.16
����           8504 Stein
 �
����Bitt� notiere� Si� au� de� Aufklebe� nebe� Ihre� Anschrif� �
����de� genaue� Rechnerty� un� all� verf}gbare� Informatione� �
����(Speicherkapazit{t� ...).
.PA�2.4�KOPIER:

����Aufruf: 
����KOPIER (Modus) (Quellaufwerk) (Ziellaufwerk) ({Optionen})

����F}r "Modus" kann angegeben werden:
����ALLES  = Kopieren der ganzen Diskette
����SYSTEM = Kopieren der Systemspuren
����DATEN  = Kopieren der Datenspuren

����F}� "Quell-� bzw�� "Ziellaufwerk� m}sse� Si� di� Name� de� �
����betreffende� logische� "CP/M"-Laufwerk� angeben� Folgend� Op�
����tione� sin� m|glich:

����A = Kein� vorherig� Sicherheitsabfrag� (u� KOPIE� beispiels�
��������weis� i� SUBMIT-File� einzusetzen)
����V = Verifizieren jeder kopierten Spur

����Bi� au� di� Optione� werde� all� nich� angegebene� Paramete� �
����vo� KOPIE� abgefragt�� Di� f}� Quell- un� Ziellaufwer� mi� �
����KONFI� eingestellte� Paramete� m}sse� }bereinstimmen� Verwen�
����de� Si� ansonste� da� CP/M-Program� PIP!
����Achtung:� KOPIEҠ kopier� nu� di� belegten� Spure� eine� Dis�
��������������kette!
����Beispiel: 

����                KOPIER DATEN A B {V}

����All� Datenspure� de� Diskett� � werde� au� di� Diskett� � �
����kopier� un� verifiziert.
.PA�..SOLLEN DIE SEITEN 17 BIS 20 GEDRUCKT WERDEN, MUSS DIE ADRESSE 7C3 
..VON WS.COM 5A ENTHALTEN, ANSONSTEN 59.
2.5 M6845:

����Aufruf:
                                M6845

����Mi�� diese�� Dienstprogram� e� is� m|glich�� di� vo�� Video�
����controlle� Baustei� MC684� verwendete� Paramete� au� de� je�
����weil� angeschlossene� Bildschir� abzustimmen�� U� dies� Ein�
����stellunge� vornehme� z� k|nnen�� m}sse� Ihne� di� horizontal� �
����un� di� vertikal� Abtastfrequen� (i� folgende� kur� fH� un� fV� �
����genannt� de� verwendete� Bildschirm� bekann� sein�� Dies� In�
����formatione� sin� normalerweis� i� de� Technische� Date� i� �
����de� de� Bildschir� beiliegende� Bedienungsanleitun� z�� fin�
����den� Weiterhi� werde� folgend� Gr|~e� ben|tigt:

    Tsl = 1 / fH 
����Tsl� bezeichne� di� horizontal� Abtastperiode�� d.h� di� Zeit� �
����di� de� Elektronenstrah� ben|tigt� u� ein� Bildschirmzeil� z� �
����zeichnen.

����Tc� is� di� L{ng� eine� Taktzyklusse� de� Videocontrolle� Bau�
����stein� 6845�� Dies� Zei� is� durc� di� Schaltun� de� Rechner� �
����fes� vorgegebe� un� betr{g� bei� GenieIII� 0,55� sec.

����Tr = 1 / fV
����Tr� bezeichne� di� vertikal� Abtastperiode� d.h� di� Zeit� di� �
����ben|tig� wird� u� ein� komplette� Bil� darzustellen.

����Nsl = ?
����Nsl� gib� di� Anzah� de� Abtastzeile� (Sca� Linien� pr� darge�
����stellte� Textzeil� an�� E� sollte� nu� Bildschirm� verwende� �
����werden� di� mindesten� el� (besse� zw|lf� Abtastzeile� zulas�
����sen� d� durc� ein� geringer� Anzah� Abtastzeile� di� Darstel�
����lungsqualit{� vo� Tex� star� geminder� wird.
.PA�����Di� einzelne� Registe� de� Videocontroller� MC684� m}sse� wi� �
����folg� programmier� werden:

����Horizontal Total Register (R0)
����R0 = Tsl / Tc - 1

����Horizontal Displayed Register (R1)
����R1 = 80

����Horizontal Sync Position Register (R2)
����R2 = ?
����Diese� Registe� bestimm� di� horizontal� Lag� de� Bilde� au� �
����de� Schirm� Sei� Wer� wir� a� beste� durc� Versuc� ermittelt� �
����Al� Ausgangsgr|~� empfiehl� e� sich�� de� Wer� de� Register� �
����R� z� nehme� (80).

����Horizontal Sync Width Register (R3)
����R3 = ?
����Diese� Registe� bestimm� di� Breit� de� horizontale� Synchro�
����nisationsimpulses�� D� dies� Impulsl{ng� be� de� Technische� �
����Date� de� Bildschirm� meisten� nich� angegebe� ist�� mu�� si� �
����durc� Versuc� ermittel� werden�� Al� Ausgangsgr|~� empfiehl� �
����e� sich� de� Wer� 1� z� nehmen� De� m|glich� Wertebereic� is� �
����� <� R� <� 15.

����Vertical Total Register (R4)
����R4 = Tr / Nsl / Tsl - 1
����Da� Resulta�� mu� mindesten� de� Wer� 24� haben�� Sollt� e� �
����kleine� al� 24 sein� mu� Nsl� u� Ein� verringer� un� R� erneu� �
����berechne� werden.

����Vertical Total Adjust Register (R5)
����R5 = (Tr - (R4 + 1) * Nsl * Tsl) / Tsl

����Vertical Displayed Register (R6)
����R6 = 25
.PA�����Vertical Sync Position Register (R7)
����R7 = ?
����Diese� Registe� bestimm� di� vertikal� Lag� de� Bilde� au� �
����de� Schirm� Sei� Wer� wir� a� beste� durc� Versuc� ermittelt� �
����Al� Ausgangsgr|~� empfiehl� e� sich�� de� Wer� de� Register� �
����R� z� nehme� (25).

����Interlace Mode Register (R8)
����R8 = ?
    0 = Normale Darstellung
����1 = Darstellung im Zeilensprungverfahren
����Da� Zeilensprungverfahre� ha� de� Vortei� eine� ausgegliche�
����nere� Helligkei� de� einzelne� Zeichen�� d� di� doppelt� An�
����zah� Abtastzeile� be� de� Bilddarstellun� benutz�� wird�� D� �
����hiermi�� allerding� ein� Halbierun� de� vertikale� Abtastfre�
����quen� (Flimmern� verbunde� ist� kan� diese� Verfahre� nu� be� �
����Monitore� mi� hohe� Nachleuchtdaue� eingesetz� werden.

����Maximum Scan Line Address Register (R9)
����R9 = Nsl - 1

����Cursor Start Register (R10)
����R10 = ?
    0       = Blockkursor
����Nsl - 1 = Unterstrichkursor

����Cursor End Register (R11)
����R11 = Nsl - 1

����M684� l{uf� men}gesteuer� a� un� kan� mi� <BREAK� abgebroche� �
����werden�� U�� eventuel� notwendig� Einstellarbeite� a�� Bild�
����schir�� z� erleichtern�� wir� vo� M684� ei� Testbil� ausge�
����geben�� Bitt� beachte� Sie� da� di� recht� K{stchenspalt� de� �
����Testbilde� ein� Schreibstell� schm{le� is� al� di� }brigen.

����Lasse Si� di� Programmierun� de� Bildschirmcontroller� �
����MC684� vo� Ihre� H{ndle� vornehmen�� fall� Si� nich� }be� ge�
����n}gend� Hardwarekenntniss� verf}gen� Ein� falsch� Programmie������run� diese� Baustein� kan� langfristi� z�� eine� Zerst|rung� �
����de� angeschlossene� Bildschirm� f}hren.

����Da� Ihne� ausgeliefert� CP/M-Syste� is� f}� de� Bildschir� �
����Sakat� SG-100� voreingestellt�� Diese� Monito� wir� vo� de� �
����Firm� TC� Compute� Gmb� al� Standardbildschir� zu� Geni� III� �
����geliefert.

����Rechenbeispie� f}� de� monochrome� Bildschir� de� IB� Perso�
����nalcomputers:

����De� Technische� Handbuc� de� IB� P� kan� entnomme� werden:

����fH = 18,432 kHz
����fV = 50 Hz
����R3 = 15

����F}r die weiteren Register gilt dann:

    Tsl = 1 / fH = 1 / 18,432 kHz = 54,25 sec
����Tr  = 1 / fV = 1 / 50 Hz      = 20 msec

����R0 = Tsl / Tc - 1 = 54,25 sec / 0,559 sec - 1 = 96

����Au� R� wir� noc� einma� Tsl� berechnet�� u� be� de� weitere� �
����Berechnunge� ein� h|her� Genauigkei� z� erzielen�� E� ergib� �
����sic� f}� Tsl� � 54,2� sec.

����R4 = Tr / Nsl / Tsl - 1 = 20 msec / 12 / 54,22 sec - 1 = 29
����R� � (Tr� - (R� + 1) � Nsl� � Tsl� � Tsl� 
����   � (2� mse� - 30 � 1� � 54,2� sec) / 54,22 sec = 9
.PA�2.6 ZEISATZ:

����Aufruf:
                               ZEISATZ

����Mi�� diese� Dienstprogram� l{~� sic� de� be� de� Bildschirm�
����darstellun� verwendet� Zeichensat� de� Geschmac� de� Benut�
����zer� anpassen�� Weiterhi� is� ein� [nderun� de� Zeichensatze� �
����z� empfehlen�� wen� mi� eine� Bildschir� gearbeite� wird� de� �
����wenige� al� el� Abtastzeile� pr� Textzeil� zul{~� (sieh� auc� �
����Kap�� 2.5� M6845)� De� bei� Genie-IIIs-CP/� eingesetzt� Stan�
����dardzeichensat�� is�� f}� ein� optimal� Darstellun� be� el� �
����ode� zw|l� Abtastzeile� ausgelegt.

����ZEISAT� l{uf� men}gesteuer� a� un� kan� mi� <BREAK�� abgebro�
����che� werden�� Bei�� Editiere� eine� Zeichen� kan� ei� Punk� �
����(Dot� mi� de� Punkttast� gesetz� bzw�� mi� de� Leertast� ge�
����l|sch� werden�� Weiterhi� lasse� sic� au~e� de� }bliche� Edi�
����tierfunktionstaste� di� Taste� F� un� F� zu�� L|sche� bzw� �
����Einf}ge� ganze� Spalte� verwenden.
.PA�3 Technische Informationen

3.1 Speicheraufteilung

����Ih� Genie-IIIs-CP/� benutz� unte� aufgef}hrt� Speicherauftei�
����lung:

����     !-----------!       !-----------!
����DFFF !           !  FFFF !           !
����     !  BNKBIOS  !       ! RESDRIVER !
����CD00 !           !  FD00 !           !
����     +-----------+       +-----------+
����CCFF !           !  FCFF !           !
����     !  BNKBDOS  !       !  RESBIOS  !
����9F00 !           !  F800 !           !
����     +-----------+       +-----------+
����9EFF !           !  F7FF !           !
����     !  BUFFER   !       !  RESBDOS  !
����2500 !           !  F200 !           !
����     +-----------+       +-----------+
����24FF !           !  F1FF !           !
����     ! BNKDRIVER !       !    TPA    !� 60.2� KByte� Benutzerbereich
����1134 !           !  0100 !           !
����     +-----------+       +-----------+
����1133 !           !  00FF !           !
����     !  SYSTAB   !       !   PAGE 0  !
����0000 !           !  0000 !           !
����     !-----------!       !-----------!

����        BANK 0              BANK 1
.PA�3.2�Ger{tezuordnung

����Nachstehend� CP/M-Ger{t� werde� vo� de� Hardwar� unterst}tzt:

����CRT  = Bildschirm und Tastatur
����LPT1 = erste  Parallelschnittstelle (Drucker)
����LPT2 = zweite Parallelschnittstelle (Drucker)
����TTY1 = erste  serielle Schnittstelle
����TTY2 = zweite serielle Schnittstelle


3.3�Tastatur

����Code-Tabelle der Sondertasten:

����        Taste       !      Bezeichnung          ! Hex  ! Dez
����                    !                           ! Code ! Code
����====================+===========================+======+=====
����        Hochpfeil   ! Start of Text             !  02  ! 002
����--------------------+---------------------------+------+-----
����SHIFT + Hochpfeil   ! Vertical Tabulation       !  0B  ! 011
����--------------------+---------------------------+------+-----
����CTRL  + Hochpfeil   ! End of Transmission Block !  17  ! 023
����--------------------+---------------------------+------+-----
����        Abw{rtspfeil! Line Feed                 !  0A  ! 010
����--------------------+---------------------------+------+-----
����SHIFT + Abw{rtspfeil! End of Text               !  03  ! 003
����--------------------+---------------------------+------+-----
����CTRL  + Abw{rtspfeil! Substitute                !  1A  ! 026
����--------------------+---------------------------+------+-----
����        Linkspfeil  ! Start of Heading          !  01  ! 001
����--------------------+---------------------------+------+-----
����SHIFT + Linkspfeil  ! Cancel                    !  18  ! 024
����--------------------+---------------------------+------+-----  
����CTRL  + Linkspfeil  ! Backspace                 !  08  ! 008
����--------------------+---------------------------+------+-----
����        Rechtspfeil ! Acknowledge               !  06  ! 006
.PA�����        Taste       !      Bezeichnung          ! Hex  ! Dez
����                    !                           ! Code ! Code
����====================+===========================+======+=====
����SHIF� � Rechtspfei� � Horizonta� Tab            �  0�  � 009
����--------------------+---------------------------+------+-----
����CTRL  � Rechtspfei� � Bell                      �  07  � 007
����--------------------+---------------------------+------+-----
����        BREAK       ! Escape                    !  1B  ! 027
����--------------------+---------------------------+------+-----
����SHIFT + BREAK       ! Zeichensatz Umschaltung
����--------------------+---------------------------+------+-----
����        CLEAR       ! Delete                    !  7F  ! 127
����--------------------+---------------------------+------+-----
����SHIFT + CLEAR       ! L|schen des Bildschirms
����--------------------+---------------------------+------+-----
����NEW LINE (ENTER)    ! Carriage Return           !  0D  ! 013
����--------------------+---------------------------+------+-----
����SHIFT + PRINT       ! Ausdrucken des Bildschirminhaltes

����Mi� SHIF� � BREA� k|nne� Si� zwische� deutsche� un� Standard-�
����ASCII-Zeichensat� umschalten.

����Be� eingeschaltete� deutsche� Zeichensat� werde� zus{tzlic� �
����folgend� Zeiche� erzeugt:

����  Taste    ! Bildschirmanzeige ! Bezeichnung !  Hex  !  Dez
    ===========!===================!=============!=======!=======
����     |     !         @         � Paragrap�   �   4�  �  064
����-----------+-------------------+-------------+-------+-------  
����SHIF� � ~  !         ^         � Zirkumfle�  !   5�  �  094
.PA�����Be� eingeschaltete� Standard-ASCII-Zeichensat� werde� zus{tz�
����lic� folgend� Zeiche� erzeugt:

����  Taste   ! Bildschirmanzeige ! Hex-Code ! Dez-Code
����==========!===================!==========!=========
����    [     !         {         �    5B    �   091
����----------+-------------------+----------+---------  
����    \     !                  �    5C    �   092
����----------+-------------------+----------+---------
����    ]     !         }         !    5D    !   093
����----------+-------------------+----------+---------
����    ~     !         ^         �    5�    �   094
����----------+-------------------+----------+---------
����SHIFT + [ !         [         !    7B    !   123
����----------+-------------------+----------+---------
����SHIFT + \ !                  !    7C    !   124
����----------+-------------------+----------+---------
����SHIFT + ] !         ~         !    7D    !   125
����----------+-------------------+----------+---------
����SHIFT + ~ !         ,         !    7E    !   126


3.3.1 WordStar Tastatur

      Be� eingeschaltete� WordSta� Tastatu� gil� folgend� �
������Belegung der Sondertasten:

����          Taste       !      Bezeichnung          ! WordStar
����                      !                           !   Code
����  ====================+===========================+=========
����          Hochpfeil   ! Kursor Zeile hoch         !   ^E
  ����--------------------+---------------------------+---------
��  ��SHIFT + Hochpfeil   ! Kursor Dateianfang        !   ^QR
����  --------------------+---------------------------+---------
  ����CTRL  + Hochpfeil   ! Kursor Schirmanfang       !   ^QE
��  ��--------------------+---------------------------+---------
����          Abw{rtspfeil! Kursor Zeile abw{rts      !   ^X
.PA�����          Taste       !      Bezeichnung          ! WordStar
����                      !                           !   Code
����  ====================+===========================+=========
��  ��SHIFT + Abw{rtspfeil! Kursor Dateiende          !   ^QC
����  --------------------+---------------------------+---------
  ����CTRL  + Abw{rtspfeil! Kursor Schirmende         !   ^QX
��  ��--------------------+---------------------------+---------
����          Linkspfeil  ! Kursor Spalte links       !   ^S
  ����--------------------+---------------------------+---------
��  ��SHIFT + Linkspfeil  ! Kursor Zeilenanfang       !   ^QS
����  --------------------+---------------------------+---------  
  ����CTRL  + Linkspfeil  ! Kursor Wort links         !   ^A
��  ��--------------------+---------------------------+---------
����          Rechtspfeil ! Kursor Spalte rechts      !   ^D
  ����--------------------+---------------------------+---------
��  ��SHIF� � Rechtspfei� � Kursor Zeilenende         �   ^QD
����  --------------------+---------------------------+---------
  ����CTRL  + Rechtspfeil ! Kursor Wort rechts        !   ^F
��  ��--------------------+---------------------------+---------
����  BREAK               ! Laufenden Bef. abbrechen  !   ^U
  ����--------------------+---------------------------+---------
��  ��        CLEAR       ! L|schen bis Zeilenende    !   ^QY
����  --------------------+---------------------------+---------
  ����SHIFT + CLEAR       ! L|schen bis Zeilenanfang  !   ^QDEL
����  --------------------+---------------------------+---------
      CTRL  + CLEAR       ! L|schen Zeichen links     !   DEL
      --------------------+---------------------------+---------
      CTRL  + Leertaste   ! Untrennbarer Leerschritt  !   ^PO
      --------------------+---------------------------+---------
����  F1                  ! Einf}gemodus ein/aus      !   ^V
  ����--------------------+---------------------------+---------
��  ��F2                  ! Einf}gen Zeile            !   ^N
      --------------------+---------------------------+---------
  ����F3                  ! L|schen Zeichen rechts    !   ^G
��  ��--------------------+---------------------------+---------
����  F4                  ! L|schen Wort              !   ^T
      --------------------+---------------------------+---------
��  ��F5                  ! L|schen Zeile             !   ^Y�����          Taste       !      Bezeichnung          ! WordStar
����                      !                           !   Code
����  ====================+===========================+=========
  ����F6                  ! Absatz formatieren        !   ^B
      --------------------+---------------------------+---------
����  F7                  ! Trennhilfe ein/aus        !   ^OH
  ����--------------------+---------------------------+---------
��  ��F8                  ! Blocksatz  ein/aus        !   ^OJ
      --------------------+---------------------------+---------
  ����P1                  ! Tabulator                 !   ^I
��  ��--------------------+---------------------------+---------
����  P2                  ! Zeile r}ckw{rts rollen    !   ^W
      --------------------+---------------------------+---------
��  ��P3                  ! Zeile vorw{rts  rollen    !   ^Z
����  --------------------+---------------------------+---------
  ����P4                  ! Seite r}ckw{rts rollen    !   ^R
      --------------------+---------------------------+---------
����  P5                  ! Seite vorw{rts  rollen    !   ^C


3.4�Bildschirm
 
����Tabelle der Bildschirmsteuercodes:

���� Hex-Code !   Dez-Code   !            Funktion
����==========+==============+===================================
����07        ! 007          ! Ausgabe eines Pieptones
����----------+--------------+-----------------------------------
����08        ! 008          ! Kursor links
����----------+--------------+-----------------------------------
����0A        ! 010          ! Kursor abw{rts
����----------+--------------+-----------------------------------
����0B        ! 011          ! Kursor hoch
����----------+--------------+-----------------------------------
����0C        ! 012          ! Kursor rechts
����----------+--------------+-----------------------------------
����0D        ! 013          ! Kursor Zeilenanfang
.PA����� Hex-Code !   Dez-Code   !            Funktion
����==========+==============+===================================
����18        ! 024          ! L|schen bis Zeilenende
����----------+--------------+-----------------------------------
����19        ! 025          ! L|schen bis Bildschirmende  
����----------+--------------+-----------------------------------
����1A        ! 026          ! L|schen des Bildschirms
����----------+--------------+-----------------------------------
����1E        ! 030          ! Kursor "home"
����----------+--------------+-----------------------------------
����1B 0C     ! 027 012      ! Kursor abschalten
  ��----------+--------------+-----------------------------------
����1B 0D     ! 027 013      ! Kursor einschalten
����----------+--------------+-----------------------------------
����1B 3D m n ! 027 061 m n  ! Kursor positionieren
 ����         !              ! (Zeile + 20H, Spalte + 20H)
����----------+--------------+-----------------------------------
    1B 41     ! 027 065      ! ASCII     Zeichensatz einschalten
    ----------+--------------+-----------------------------------
    1B 46 n   ! 027 070 n    ! Bildschirmfenster selektieren
����          !              ! (0 <= n <= 7 {+ 20H})
����----------+--------------+-----------------------------------
    1B 47     ! 027 071      ! Deutschen Zeichensatz einschalten
    ----------+--------------+-----------------------------------
����1B 49 n   ! 027 073 n    ! Setzen der obersten  Zeile (+ 20H)
����----------+--------------+-----------------------------------
����1B 4A n   ! 027 074 n    ! Setzen der untersten Zeile (+ 20H)
����----------+--------------+-----------------------------------
����1B 4B n   ! 027 075 n    ! Setzen der linken   Spalte (+ 20H)
    ----------+--------------+-----------------------------------
����1B 4C n   ! 027 076 n    ! Setzen der rechten  Spalte (+ 20H)
����----------+--------------+-----------------------------------
    1B 4E     ! 027 078      ! Einschalten der Standardtastatur
    ----------+--------------+-----------------------------------
    1B 4F     ! 027 079      ! Einschalten der WordStar Tastatur
    ----------+--------------+-----------------------------------
����1B 50     ! 027 080      ! Zeichen einf}gen
.PA����� Hex-Code !   Dez-Code   !            Funktion
����==========+==============+===================================
����1B 51     ! 027 081      ! Zeichen l|schen
����----------+--------------+-----------------------------------
����1B 52     ! 027 082      ! Inverse Darstellung einschalten
����----------+--------------+-----------------------------------
����1B 53     ! 027 083      ! Inverse Darstellung abschalten
����----------+--------------+-----------------------------------
����1B 56     ! 027 086      ! Zeile einf}gen
����----------+--------------+-----------------------------------
����1B 57     ! 027 087      ! Zeile l|schen
    ----------+--------------+-----------------------------------
����1B 58     ! 027 088      ! autom. Zeilenumbruch einschalten
����----------+--------------+-----------------------------------
����1B 59     ! 027 089      ! autom. Zeilenumbruch abschalten


����Tabelle des Graphikzeichensatzes:

         Hex-Code ! Dez-Code !    dargestelltes Zeichen    
         =========+==========+=============================
���� ����   80    !   128    ! senkrechter Balken
         ---------+----------+-----------------------------
            81    !   129    ! waagrechter Balken
         ---------+----------+-----------------------------
            82    !   130    ! Kreuzung
         ---------+----------+-----------------------------
���� ����   83    !   131    ! Abzweigung links
         ---------+----------+-----------------------------
���� ����   84    !   132    ! Abzweigung rechts
         ---------+----------+-----------------------------
���� ����   85    !   133    ! Abzweigung oben
         ---------+----------+-----------------------------
���� ����   86    !   134    ! Abzweigung unten
         ---------+----------+-----------------------------
���� ����   87    !   135    ! rechte untere Ecke
         ---------+----------+-----------------------------
���� ����   88    !   136    ! linke  untere Ecke�         Hex-Code ! Dez-Code !    dargestelltes Zeichen    
         =========+==========+=============================
���� ����   89    !   137    ! rechte obere  Ecke
         ---------+----------+-----------------------------
���� ����   8A    !   138    ! linke  obere  Ecke
         ---------+----------+-----------------------------
���� ����   8B    !   139    ! rechte untere Ecke (gebogen)
         ---------+----------+-----------------------------
���� ����   8C    !   140    ! linke  untere Ecke (gebogen)
         ---------+----------+-----------------------------
���� ����   8D    !   141    ! rechte obere  Ecke (gebogen)
         ---------+----------+-----------------------------
���� ����   8E    !   142    ! linke  obere  Ecke (gebogen)
         ---------+----------+-----------------------------
            8F    !   143    ! Hochpfeil
         ---------+----------+-----------------------------
            90    !   144    ! Abw{rtspfeil
         ---------+----------+-----------------------------
            91    !   145    ! Linkspfeil
         ---------+----------+-----------------------------
            92    !   146    ! Rechtspfeil
         ---------+----------+-----------------------------
            93    !   147    ! Pik
         ---------+----------+-----------------------------
            94    !   148    ! Herz
         ---------+----------+-----------------------------
            95    !   149    ! Karo
         ---------+----------+-----------------------------
            96    !   150    ! Kreuz
         ---------+----------+-----------------------------
            97    !   151    ! halbhelle Fl{che
         ---------+----------+-----------------------------
            98    !   152    ! Copyright Zeichen 
         ---------+----------+-----------------------------
            99    !   153    ! Hand (linker    Teil)
         ---------+----------+-----------------------------
            9A    !   154    ! Hand (mittlerer Teil)
.PA�         Hex-Code ! Dez-Code !    dargestelltes Zeichen    
         =========+==========+=============================
            9B    !   155    ! Hand (rechter   Teil)   
         ---------+----------+-----------------------------
            9C    !   156    ! Pi
         ---------+----------+-----------------------------
            9D    !   157    ! Omega
         ---------+----------+-----------------------------
            9E    !   158    ! lachendes Gesicht
         ---------+----------+-----------------------------
            9F    !   159    ! weinendes Gesicht 
          
����U�� gr|~te� Komfor� be� de� Programmierun� vo� Bildschirmmas�
����ke� z�� bieten��� wurd� jetz�� zus{tzlic� ei� sogenannte� �
����"window"-Modu� geschaffen� de� e� erm|glicht� de� Ein-/Ausga�
����bebereic� de� Bildschirm� einzugrenzen�� wobe� Kopf- un� Fu~�
����zeil� bzw�� link� un� recht� Spalt� fre� gew{hl� werde� k|n�
����nen�� Au� dies� Weis� kan� mi� bi� z� ach� Bildschirmfenster� �
����verfahre� werden.

����Is�� de� E/A-Bereic� einma� au� dies� Weis� eingegrenz�� wor�
����den�� is� e� nich� meh� m|glich�� Bildschirmpositione� au~er�
����hal� de� angegebene� Fenster� z� beschreiben�� All� Steuer�
����funktione� wi� "L|sch� Zeil� bzw�� Zeichen"� "F}g� Zeil� bzw� �
����Zeiche� ein"�� L|sche� de� Bildschirm� usw�� beziehe� sic� �
����jetz�� nu� noc� au� diese� Fenster�� Befinde� sic� de� Kurso� �
����vo� de� Definitio� de� Bildschirmfenster� au~erhal� diese� �
����Fensters�� wir� e� nac� Eingrenze� de� Bildschirm� i� diese� �
����Fenste� "hineingezogen"�� Au� de� Systemdiskett� is� i� de� �
����Programmiersprach� "C�� de� Quellcod� eine� Routin� namen� �
����"window� beigef}gt� di� rech� eindrucksvol� di� M|glichkeite� �
����diese� "window"-Modu� demonstriert�� Dies� Routin� wir� auc� �
����be� de� men}gesteuerte� Dienstprogramme� FTASTEN�� KONFI� un� �
����ZEISAT� eingesetzt.
.PA�3.5 Serielle Schnittstelle

3.5.1 ]bertragung digitaler Daten

������]be� relati� lang� Distanze� werde� digital� Date� generel� �
������i� serieller� For� }bertragen�� wobe� ein� einfach� zweipo�
������lig� Leitun� Sende- un� Empfangsger{� miteinande� verbin�
������det� E� gib� zwe� ]bertragungstechniken� asynchron� un� syn�
������chron�� Di� seriell� Schnittstell� Ihre� Geni� III� System� �
������}bertr{g� Date� asynchro� un� bit-seriell� Asynchron� ]ber�
������tragun� ben|tig� keine� Tak� zu� Synchronisierung�� un� de� �
������Datenstro� mu� nich� kontinuierlic� sein� Da� bedeutet� da� �
������zwische� de� ]bertragun� individuelle� Zeiche� Pause� be�
������liebige� L{ng� auftrete� d}rfen.

������Ei� individuelle� Zeiche� besteh� au� eine� Datenwor�� (ge�
������w|hnlic� f}n� bi� ach� Bit� lang� un� synchronisierende� �
������Start- un� Stopelementen� Da� Startelemen� is� ein� einzel�
������n� logisch� Nul� (ein Bit)�� di� vo� de� Datenwor�� steht� �
������Da� Stopelemen� wir� solang� }bertragen�� bi� da� n{chst� �
������Startelemen� anschlie~t� E� gib� keine ober� Grenz� f}� di� �
������L{ng� de� Stopelements� E� existier� jedoc� ei� untere� Li�
������mit�� welche� vo� de� Systemeigenschafte� de� angeschlosse�
������ne� Ger{t� abh{ngt�� Typisch� Untergrenze� sin� 1.0�� 1.4� �
������bzw�� 2.� Bit� L{ng� (di� meiste� moderne� System� benutze� �
������ei� ode� zwe� Stopbits).

������De� negativ� ]bergan� de� Startelement� definier� di� Posi�
������tio� de� einzelne� Bit� i� z� }bertragende� Datenwort� Ein� �
������intern� Uh� i� Empf{nge� wir� be� diese� ]bergan� gesetz� �
������un� daz� verwendet�� di� Positio� de� Datenbit� z�� lokali�
������sieren.

������E� gib� einig� gut� Gr}nde�� di� asynchron� ]bertragungs�
������techni� z� benutzen�� E� mu� kei� Taktsigna� mi� de� Daten�
������wor� }bertrage� werden�� wa� z� eine� Vereinfachun� de� be�
������n|tigte� Hardwar� f}hrt�� Auc� m}sse� di� z�� }bertragende� �
������Zeiche� nicht� hintereinande� gesende� werden�� si� werde� ��������}bertragen�� wen� si� verf}gba� sind�� Die� is�� besonder� �
������n}tzlich�� wen� Date� vo� eine� Ger{� mi� manuelle� Eingab� �
������}bertrage� werde� (z.B�� eine� Tastatur)� De� Hauptnachtei� �
������de� asynchrone� ]bertragungstechni� ist�� da� ei� nich� un�
������erhebliche� Tei� de� Kommunikationsbandbreit� f}� Start- �
������un� Stopelement� verbrauch� wird.

������Di� Frequenz�� mi� de� asynchron� Date� }bertrage� werden� �
������wir� al� Baudrate� bezeichnet�� Di� Baudrat� is� di� Umkeh�
������run� de� ]bertragungsdaue� de� k}rzeste� Signalelements� �
������normalerweis� ei� Datenbitintervall�� Wen� ei� Stopbi� ver�
������wende� wird�� entsprich� di� Baudrat� de� Bitrate�� be� Sy�
������stemen�� di� meh� al� ei� Stopbi� verwenden� entsprich� di� �
������Baudrat� nich� meh� de� Bitrate.
    

����  ------�  +----�    +--�      +-------�      +--�          +--
����    ^!   !  !    !    !  !      !   ^!   !      !  !          !
����    !   +--+    +----+  +------+   !   +------+  +----------+
����    !    ^! ^!___________________^!   !    ^! ^!_________________^!
����  Stop-  !           !           Stop-  !          ! 
����  element!    8-Bit Datenwort    element!   8-Bit Datenwort
����         !      (11001000)              !     (00100000)
����      Start-                         Start-
����      element                        element
                          Asynchrone Daten

      Asynchron� ]bertragun� }be� ein� einfach� zweipolig� Lei�
������tun� kan� mi� mittelhohe� Baudrate� durchgef}hr� werde� (1� �
������KBau� ode� mehr�� abh{ngi� vo� de� Leitungsl{ng� un� de� �
������Treiberhardware�� Be� ]bertragun� }be� ei� Telefonnet�� is� �
������di� Baudrat� au� zirk� � KBau� begrenzt�� un� ei� Mode� is� �
������erforderlich�� u� di� Datenimpuls� i� analog� Date� (T|ne)� �
������di� }be� ei� Telefonnet� }bertrage� werde� k|nnen� umzuwan�
������deln.
.PA�3.5.2 Signalpegel

������Di� E.I.A.-Norme� f}� di� RS-232-� Schnittstell� definiere� �
������di� Spannnungspege� un� ihr� zugeh|rige� logische� Zust{nd� �
������f}� de� Austausc� vo� Date� un� Steuerinformatione� zwi�
������sche� de� miteinande� kommunizierende� Ger{ten.

������Bei�� Datenaustausc� entsprich� ei� Signa� eine� logische� �
������Eins�� wen� a� de� Schnittstell� ein� Spannung� di� niedri�
������ge� al� minu� dre� Vol� is� (bezoge� au� Masse)�� gemesse� �
������wird� un� eine� logische� Null� wen� di� Spannun� h|he� al� �
������plu� dre� Vol� ist� Be� de� sogenannte� "Handshake� Leitun�
������ge� entsprich� di� negativ� Spannun� de� Zustan� "Aus�� un� �
������di� positiv� Spannun� de� Zustan� "Ein"�� Nachstehend� Ta�
������bell� fa~� noc� einma� zusammen:

            !===================!=========================!
            !                   !  ]bertragungsspannung   !
            !     Notation      +------------+------------+
            !                   !   negativ  !  positiv   !
            !===================!============!============!
            ! Logischer Zustand !      1     !     0      !
            +-------------------+------------+------------+
            !   Signalzustand   !  "Marking" ! "Spacing"  !
            +-------------------+------------+------------+
            !     Funktion      !     AUS    !    EIN     !
����        +-------------------+------------+------------+
����        !  physik. Pegel    !-15V -> -3V !+3V -> +15V !
            !===================!============!============!
.PA�3.5.3 Anschlu~bezeichnungen und Signalbeschreibungen

������Al� Anschlu�� f}� di� seriell� Schnittstell� is�� ei� 25-�
������polige� Stecke� genorm� (DB-25)� Folgend� Tabell� f}hr� di� �
������Anschlu~belegun� auf:

DTE ! Pin ! DIN-Bezeichnung           ! EIA-Bezeichnung              ! CCITT! DCE
----+-----+---------------------------+------------------------------+------+----
--- !  1  ! E1   Schutzerde           ! PGND Protective ground     AA! 101  ! ---
----+-----+---------------------------+------------------------------+------+----
--> !  2  ! D1   Sendedaten           ! TD   Transmit Data         BA! 103  ! -->
----+-----+---------------------------+------------------------------+------+----
<-- !  3  ! D2   Empfangsdaten        ! TD   Receive Data          BB! 104  ! <--
----+-----+---------------------------+------------------------------+------+----
--> !  4  ! S2   Sendeteil einschalten! RTS  Request To Send       CA! 105  ! -->
----+-----+---------------------------+------------------------------+------+----
<-- !  5  ! M2   Sendebereitschaft    ! CTS  Clear To Send         CB! 106  ! <--
----+-----+---------------------------+------------------------------+------+----
<-- !  6  ! M1   Betriebsbereitschaft ! DSR  Data Set Ready        CC! 107  ! <--
----+-----+---------------------------+------------------------------+------+----
--- !  7  ! E2   Betriebserde         ! SGND Signal Ground         AB! 102  ! ---
----+-----+---------------------------+------------------------------+------+----
<-- !  8  ! M5   Empfangssignal       ! DCD  Data Channel Received CF! 109  ! <--
    !     !                           !      Line Signal Detector    !      ! 
----+-----+---------------------------+------------------------------+------+----
--> ! 20  ! S1.2 Endger{t betr.bereit ! DTR  Data Terminal Ready   CD! 108.2! -->
----+-----+---------------------------+------------------------------+------+----

������DT�     Dat� Termina� Equipmen� (Dat� Source� Dat� Sink)
������� DE�   Datenendeinrichtun� (Datenquelle� Datensenke)

������DC�     Dat� Communication� Equipmen� (Modem)
������� D]�   Daten}bertragungseinrichtun� (Modem)
.PA�������Signalbeschreibung

������Schutzerd� (Protectiv� Ground):� Si� mu� mi�� de�� Ger{te�
������chassi� verbunde� sein�  Si� kan� auc� mi� de� "Signalerde/�
������Betriebserde� verbunde� sein.

������Sendedate� (Transmi� Data):� Diese� Signa� mu� w{hren� de� �
������Intervall� zwische� de� einzelne� Zeiche� un� de� Zeit�� i� �
������de� kein� Date� gesende� werden�� i� "Marking� Zustan� ge�
������halte� werden.

������Empfangsdate� (Receiv� Data):� Diese� Signa� mu� w{hren� de� �
������Intervall� zwische� de� einzelne� Zeiche� un� de� Zeit�� i� �
������de� kein� Date� gesende� werden�� vo� angeschlossene� Ger{� �
������i� "Marking� Zustan� gehalte� werden.

������Sendetei� einschalte� (Request-to-send):   Be� Einweg- ode� �
������Vollduplex}bertragun� kennzeichne�� de� "Ein� Zustan� di� �
������Sendebereitschaf� de� Schnittstelle.
������Be� Halbduplex}bertragun� kennzeichne� de� "Ein�� Zustan� �
������di� Sendebereitschaf�� un� keine� Empfangsbereitschaf�� de� �
������Schnittstelle� De� "Aus� Zustan� kennzeichne� di� Empfangs�
������bereitschaf� de� angeschlossene� Ger{tes.

������Sendebereitschaf�� (Clear-to-send):� Diese� Signa� wir� vo� �
������angeschlossene� Ger{� generier� un� zeig� an�� o� e� berei� �
������ist�� Date� z�� empfangen�� De� "Ein�� Zustan� zeig�� de� �
������Schnittstell� an�� da� da� angeschlossen� Ger{� Date� emp�
������fange� kann�� De� "Aus� Zustan� zeig� de� Schnittstell� an� �
������da� da� angeschlossen� Ger{� nich� empfangsberei� ist.

������Betriebsbereitschaf� (Dat� Se� Ready):  Diese� Signa� zeig� �
������de� Statu� de� angeschlossene� Ger{te� an�� wobe� de� "Ein� �
������Zustan� Kommunikationsbereitschaf� signalisiert�� De� "Aus� �
������Zustan� trit� z� alle� andere� Zeite� au� un� zeig� an� da� �
������da� angeschlossen� Ger{� all� Signal� de� Schnittstell� ig�
������noriert.
�������Empfangssignalpege� (Carrie� Detect):� De� "Aus�� Zustan� �
������zeig� an�� da� di� Signalqualit{� nich� ausreich� f}� ein� �
������einwandfrei� Daten}bertragung��� Diese� Signa� wir� vo� �
������GenieIII� nich� ausgewertet.

������Termina� betriebsberei� (Dat� Termina� Ready):� De� "Ein� �
������Zustan� zeig� de� angeschlossene� Ger{� di� Betriebsbereit�
������schaf� de� Schnittstell� an.


3.5.4 Anschlu~belegung

������Geni� CP/� is� al� DT� (Dat� Termina� Equipment� ausgeleg� �
������(link� Seit� i� o.a� Bild).

������Wen� zwe� Ger{t� al� DT� ausgeleg� sind�� m}sse� si� }be� �
������ei� sogenannte� Nullmode� verbunde� werden� Hie� werde� di� �
������Leitungspaar� (� - 3)�� (� - 5� un� (�� - 20�� miteinande� �
������vertauscht.

������Nullmode� mi� Software-Protokoll

������Geni�                               DTE
������---------------------------------------

������  �   ----------------------------   1
������  �   ------------ /-------------   3
������                   X
������  �   ------------/ -------------   2
������  �   ----
������  �   ----/
������  7   -----------------------------   7
������  6   ----
������ 2�   ----/
.PA�����Nullmode� mi� Hardware-Protokoll

����Geni�                               DTE
����---------------------------------------

����  �   ----------------------------   1
����  �   ------------ /-------------   3
����                   X
����  �   ------------� -------------   2
����  �   ------------ /-------------   5
����                   X
����  �   ------------� -------------   4
����  �   ----------------------------   7
����  �   ------------ /-------------  20
����                   X
���� 2�   ------------� -------------   6
.PA �4 Treiberroutinen

��I�� Genie-IIIs-CP/� sin� all� physischen� Treiberroutine� stren� �
��vo� BIO� getrennt�� Dies� Treiberroutine� k|nne� }be� eine� zu�
��s{tzliche� BIO� Vektor�� de� hinte� de� XMOV� Aufru� de� Origi�
��na� BIO� liegt�� erreich� werden�� Bi� au� de� Akkumulator� de� �
��al� R}ckgaberegiste� verwende� wird�� werde� s{mtlich� Prozes�
��sorregiste� be� Aufru� diese� Treiberroutine� gerettet.

��Di� einzelne� Treiberroutine� werde� mittel� eine� Funktions�
��numme� unterschieden� di� i� Registe� � z� }bergebe� ist.

��Beispiel f}r den Aufruf der Treiberroutinen:

��WBOOT   EQU  0000H               ;warm boot entry point
��USERF   EQU  30                  ;additional BIOS function

��SYSTEM: PUSH BC                  ;save reg. BC
��        L�   IX,(WBOOT+1�        ;war� boo� entr� poin�        --� reg. IX
��        L�   BC,3*(USERF-1�      ;offse� t� syste� entr� poin� --> reg. BC
��        ADD  IX,BC               ;add offset
��        POP  BC                  ;restore reg. BC
��        J�   (IX�                ;perfor� syste� cal� an� retur� t� caller


��Beschreibung aller Treiberroutinen:


��Funktion 0: Videocontroller initialisieren

��Import: HL = ^Videoparametertabelle

��Di� Parametertabell� mu� mindesten� 1� Byte� lan� sein�� d� di� �
��Registe� R� bi� R1� de� Controller� komplet� gelade� werden.

��Diese� Aufru� sollt� nu� vo� erfahrenen�� Systemprogrammierer� �
��verwende�� werden�� deshal� wir� au� ein� Beschreibun� de� �
��Controllerregiste� hie� nich� weite� eingegangen�� Ein� detail����liert� Beschreibun� de� Controllerregiste� finde� Si� i� Daten�
��blat� de� Videocontroller� MC6845� da� be� Motorol� angeforder� �
��werde� kann.


��Funktion 1: RS232-C Schnittstelle initialisieren

��Import: A = Datenformat
��        B = Nummer der Schnittstelle
��            0 = erste  serielle Schnittstelle (SIO A)
��            1 = zweite serielle Schnittstelle (SIO B) 
��        E = Baudrate

��Mi� diese� Aufru� is� e� m|glich�� da� Daten}bertragungsforma� �
��un� di� Baudrat� de� serielle� Schnittstell� einzustellen.

��Beschreibung der ]bergabeparameter:

��Datenformat
��Bit 0    :�Wen� diese� Bi� logisc� Ein� ist� wir� ei� Parit{ts�
�������������bi� generier� (Senden� bzw� abgepr}f� (Empfangen).
  
��Bit 1    : Wen� diese� Bi� logisc� Ein� ist� wir� da� Parit{ts�
�������������bi� be� eine� gerade� Anzah� vo� logische� Einse� i� �
�������������Datenwor� generiert�� ansonste� be� eine� ungerade� �
�������������Anzahl.

��Bits 2, 3:�Dies� beide� Bit� gebe� di� Anzah� de� Stopbit� i� �
�������������jede� z� }bertragende� Zeiche� an� Dabe� gilt:

                     Bit 3 ! Bit 2 ! Stopbits
                     ------+-------+---------
                        0  !   1   !    1
                        1  !   0   !   1,5
                        1  !   1   !    2
.PA���Bits 7, 6: Dies� beide� Bit� gebe� di� Anzah� de� Bit� i� jede� �
�������������z�� sendende� ode� z� empfangende� serielle� Daten�
�������������wor� an� Dabe� gilt:

                          Bit 7 ! Bit 6 ! Wortl{nge
                          ------+-------+----------
                            0   !   0   !  5 Bits
                            0   !   1   !  6 Bits
                            1   !   0   !  7 Bits
                            1   !   1   !  8 Bits


��Baudrate��� 
��Hiermi�� l{~�� sic� di� gew}nscht� Baudrat� einstellen�� wobe� �
��gilt:

                         Baudrate ! E 
                         ---------+---
                              50  !  2
                              75  !  3
                             110  ! 15
                           134,5  !  4
                             150  ! 14
��                           200  !  5
                             300  ! 13
                             600  !  6
                            1200  ! 11
                            1800  ! 10
                            2400  !  7
                            4800  !  9
                            9600  !  8
                           19200  !  0


��Funktion 2: Tastaturstatus

��Export: A = ASCII Code der gedr}ckten Taste
��            (0 = keine Taste gedr}ckt)���Funktion 3: Tastatureingabe

��Export: A = gelesenes Zeichen (ASCII Code)


��Funktion 4: Bildschirmausgabe

��Import: A = auszugebendes Zeichen (ASCII Code)


��Funktion 5: Druckerstatus

��Import: B = Nummer des Druckers
��            0 = erste  Parallelschnittstelle
��            1 = zweite Parallelschnittstelle (PIO)

��Export: A = 0   Drucker bereit
��         <> 0   Drucker nicht bereit


��Funktion 6: Druckerausgabe

��Import: A = auszugebendes Zeichen
��        B = Nummer des Druckers


��Funktion 7: RS 232-C Eingabestatus

��Import: B = Nummer der Schnittstelle

��Export: A = 0   kein Zeichen empfangen
��         <> 0   Zeichen empfangen
.PA���Funktion 8: RS 232-C Eingabe

��Import: B = Nummer der Schnittstelle

��Export: A = empfangenes Zeichen


��Funktion 9: RS 232-C Ausgabestatus

��Import: B = Nummer der Schnittstelle

��Export: A = 0   Empf{nger bereit
��         <> 0   Empf{nger nicht bereit


��Funktion 10: RS 232-C Ausgabe

��Import: A = zu sendendes Zeichen
��        B = Nummer der Schnittstelle
.PA���Funktion 11: Diskettensektor lesen

��Import:  A = Banknummer des Datenbuffers (oberes Nibble)
�������������  Laufwerksnummer       (0-7, unteres Nibble)
��         B = Sektornummer
��         E = Spurnummer
��        HL = ^Datenbuffer

��Export:  A = Fehlerstatus
��             0 = kein Fehler
��             1 = ung}ltige Laufwerksnummer
��             2 = ung}ltige Spur
��             3 = ung}ltiger Sektor
��             4 = Laufwerk nicht bereit
��             6 = Datenrecord gel|scht/gesperrt
               7 = Datenrecord nicht gefunden
               8 = CRC-Fehler
               9 = Daten verloren

��Di� Charakteristik� de� z� lesende� Diskett� k|nne� i�� SYSTA� �
��Bereic� (sieh� Anhan� A� eingestell�� werden�� Diese� Aufru� �
��sollt� jedoc� nu� vo� erfahrenen� Programmierer� verwende�� wer�
��den.
.PA���Funktion 12: Diskettensektor schreiben
 
��Import:  A = Banknummer des Datenbuffers (oberes Nibble)
�������������  Laufwerksnummer       (0-7, unteres Nibble)
��         B = Sektornummer
��         E = Spurnummer
��        HL = ^Datenbuffer

��Export:  A = Fehlerstatus
��             0 = kein Fehler
��             1 = ung}ltige Laufwerksnummer
��             2 = ung}ltige Spur
��             3 = ung}ltiger Sektor
��             4 = Laufwerk nicht bereit
��             5 = Laufwerksfehler
��             6 = Diskette schreibgesch}tzt
               7 = Datenrecord nicht gefunden
               8 = CRC-Fehler
               9 = Daten verloren


��Funktion 13: Datum und Uhrzeit lesen (nur Hardwareuhr)

��Import:�HL = Adress� eine� 2� Byte� lange� Buffers�� de� be� �
���������������R}ckkeh� zu�� Aufrufe� Datu� un� Uhrzei�� i� de� �
���������������For� WW� MM/DD/Y� HH:MM:S� enth{lt� wobe� gilt:

���������������          WWW = Wochentag
���������������          MM  = Monat
���������������          DD  = Tag
���������������          YY  = Jahr
���������������          HH  = Stunden
���������������          MM  = Minuten
���������������          SS  = Sekunden
.PA���Funktio� 14� Datu� un� Uhrzei� setzen (nur Hardwareuhr)

��Import� � � Wochenta� (0-6� � � Sa)
����      � � Mona�     (MM)
����      � � Stunde�   (HH)
����      � � Minute�   (MM)
����      � � Ta�       (TT)
����      � � Jah�      (JJ)

��S{mtlich� Date� werde� i� BCD-Forma� }bergeben� 
��Be� de� Zehnerstell� de� Stunde� mu� Bi� � gesetz�� sein�� I� �
��Fall� eine� Schaltjahre� mu� Bi� � be� de� Zehnerstell� de� �
��Tage� gesetz� sein.


��Funktion 15: Interbank Transfer

��Import:  A  = Quellbank (oberes  Nibble)
����            Zielbank  (unteres Nibble)
����       B  = Anzah� de� z� transferierende� Byte� (128 max.)
���������� DE = Zieladresse
����       HL = Quelladresse
.PA���Funktion 16: Hard Disk Sektor lesen

��Import:  A = Banknummer des Datenbuffers (oberes Nibble)
�������������  Laufwerksnummer       (0-2, unteres Nibble)
��         B = Sektornummer
��        DE = Spurnummer
��        HL = ^Datenbuffer

��Export:  A = Fehlerstatus
��             0 = kein Fehler
��             1 = ung}ltige Laufwerksnummer
��             2 = ung}ltige Spur
��             3 = ung}ltiger Sektor
��             4 = Laufwerk nicht bereit
               7 = Datenrecord nicht gefunden
               8 = CRC-Fehler

��Di� Charakteristik� de� z� lesende� Festplattenlaufwerk� k|nne� �
��i� SYSTA� Bereic� (sieh� Anhan� A� eingestell� werden� Di� Sek�
��tore� werde� absolu� gelesen� d.h� e� wir� kein� Verwaltun� vo� �
��defekte� Sektore� durchgef}hrt�� Diese� Aufru� sollt� nu� vo� �
��erfahrenen� Programmierer� verwende� werden.
.PA���Funktion 17: Hard Disk Sektor schreiben
 
��Import:  A = Banknummer des Datenbuffers (oberes Nibble)
�������������  Laufwerksnummer       (0-2, unteres Nibble)
��         B = Sektornummer
��        DE = Spurnummer
��        HL = ^Datenbuffer

��Export:  A = Fehlerstatus
��             0 = kein Fehler
��             1 = ung}ltige Laufwerksnummer
��             2 = ung}ltige Spur
��             3 = ung}ltiger Sektor
��             4 = Laufwerk nicht bereit
��             6 = Laufwerksfehler (Wechselplatte schreibgesch}tzt)
               7 = Datenrecord nicht gefunden
               8 = CRC-Fehler


��Funktion 18: Datum und Uhrzeit im CP/M 3 Format lesen

��Import:� HL� = Adress� eine� � Byte� lange� Buffers�� de� be� �
�����������������R}ckkeh� zu� Aufrufe� Datu� un� Uhrzei� i� de� �
�����������������For� TTTTHHMMS� enth{lt� wobe� gilt:

�����������������TTTT = Anzahl der Tage seit dem 1.Januar 1978  
�����������������       (bin{r) 
�����������������HH   = Stunden  (BCD)
�����������������MM   = Minuten  (BCD)
�����������������SS   = Sekunden (BCD)


  Funktion 19: Datum und Uhrzeit im CP/M 3 Format setzen

��Import: D  = Stunden (BCD)
����      E  = Minuten (BCD)
����      HL = Anzahl der Tage seit dem 1. Januar 1978 (bin{r)
.PA���Funktion 20: Bitmuster eines Bildschirmzeichens laden

��Import: A  = ASCII-Code des zu ladenden Zeichens (20 - 9F)
����      HL = ^Bitmuster (16 Bytes)

��Da� erst� Byt� i� Buffe� entsprich� de� erste� Abtastzeil� �
��(Sca� Linie� de� Bildschirmzeichens�� da� zweit� Byt� de� zwei�
��te� Abtastzeil� usw.� Di� ach� Bit� eine� Byte� entspreche� de� �
��Punkte� (Dots)� au� dene� sic� ein� Abtastzeil� de� Bildschirm�
��zeichen� zusammensetzt�� wobe� dies� Punkt� (Bits�� seitenver�
��kehrt� i� Buffe� abgespeicher� sei� m}ssen.


��Funktion 21: Originalzeichensatz wiederherstellen
.PA���Funktion 22: Direkte Bildschirmfensterein-/ausgabe

��Import: A  = 0 Bildschirm lesen
����           1 Bildschirm schreiben
����      D  = 0 zusammenh{ngender Bildfensterbuffer
����             (Buffe� mu� mindesten� sovie� Byte� lan� sein� �
�����������������wi� da� momenta� aktiviert� Bildschirmfenste� �
�����������������Zeiche� beinhaltet)
����           1 "Full screen" Buffer
  ��             (Buffe� enth{l�� vollst{ndig� Bildschirmseite�                �
�����������������d.h�� sein� L{ng� mu� mindesten� 192� Byte� be�
�����������������tragen)
����      E  = 0 normale Darstellung
����           1 inverse Darstellung
����           (nur bei Bildschirmausgabe relevant)
����      HL = ^Benutzerbuffer

��Dies� Funktio� bezieh� sic� au� da� momentan� aktiviert� Bild�
��schirmfenster�� wobe� imme� nu� de� komplett� Fensterinhal� ge�
��lese� bzw�� geschriebe� werde� kann�� Wurd� mi� D=� de� "Ful� �
��screen�� Buffe� gew{hlt�� s� greif� di� Treiberroutin� j� nac� �
��Gr|~� un� Lag� de� aktuelle� Bildschirmfenster� automatisc� au� �
��di� richtig� Bufferpositio� zu� d.h� i� H� mu� be� � � � grund�
��s{tzlic� nu� di� Anfangsadress� eine� vollst{ndige� Bildschirm�
��seit� angegebe� werden� Dies� Optio� erspar� de� Anwendungspro�
��grammiere� bei� Aufba� vo� komplizierte� Bildschirmmaske� vie� �
��Rechenarbeit.


��Funktion 23: Allgemeine Hardwareinitialisierung

��- De� Bildschirmcontrolle� wir� mi� de� Powerup/Rese�� Parame�
����ter� initialisiert.
��- Di� serielle� Schnittstelle� werde� mi� de� Powerup/Rese� Pa�
����rameter� initialisiert.
��- De� Z8� PI� Baustei� wir� s� initialisiert� da� e� al� zweit� �
����Parallelschnittstell� fungiere� kann.
��- Der Real Time Clock Interrupt Vektor wird gesetzt.���Funktio� 24� Uhrenanzeig� ein-/ausschalten

��Import� � � � Uhrenanzeig� ausschalten
��         <> 0 Uhrenanzeige einschalten
��        B = Zeilennummer  der Uhrenanzeige (0 - 24)
��        E = Spaltennummer der Uhrenanzeige (0 - 79)


��Funktion 25: Graphikbildschirm ein-/ausschalten

��Import: A = 0 Graphikbildschirm ausschalten
��         <> 0 Graphikbildschirm einschalten
��        B = Nummer der anzuzeigenden Graphikseite (0 oder 1)


��Funktion 26: Graphikbildschirm l|schen

��Import: A = Farbe
��            0 = schwarz
��            1 = wei~
��        B = Nummer der zu l|schenden Graphikseite (0 oder 1)
.PA���Funktion 27: Punkt auf Graphikbildschirm setzen

��Import: A  = 0 keine Normierung der Koordinaten (oberes Nibble)
��             1       Normierung der Koordinaten (oberes Nibble)
��             Farbe (unteres Nibble)
��             0 = schwarz
��             1 = wei~
��        B  = Nummer der anzusprechenden Graphikseite (0 oder 1)
 �        DE = x-Koordinate (0 - 639)
��        HL = y-Koordinate (0 - NNN  nicht normiert)
��                          (0 - 449        normiert)
��                            
��D� da� Seitenverh{ltni� vo� x- un� y-Achs� nich� de� Seitenver�
��h{ltni� de� Bildschirmkante� entspricht�� besteh� di� M|glich�
��kei�� eine� normierte� Graphikausgabe�� d.h�� di� y-Koordinat� �
��wir� entsprechen� de� tats{chliche� vertikale� Aufl|sun� ska�
��liert�� Voreingestell�� sin� 1� Abtastzeile� pr� dargestellte� �
��Textzeil� (sieh� auc� Kap�� 2.5�� M6845)�� da� ergib� insgesam� �
��27� Abtastzeilen�� Be� normierte� Ausgab� wir� nu� di� zwische� �
���� un� 44� liegend� y-Koordinat� au� eine� zwische� �� un� 27� �
��liegende� Wer� abgebildet�� Dadurc� wir� bewirkt�� da� de� Ab�
��stan� zwische� zwe� Punkte� au� de� y-Achs� derselb� is�� wi� �
��de� zwische� zwe� Punkte� au� de� x-Achse.


��Funktion 28: Punkt von Graphikbildschirm lesen

  Import: B  = Nummer der anzusprechenden Graphikseite (0 oder 1)
��        DE = x-Koordinate (0 - 639)
��        HL = y-Koordinate (0 - NNN)

��Export: A  = Farbe des gelesenen Punktes
��             0 = schwarz
��             1 = wei~
.PA���Funktion 29: Gerade zeichnen

��Import: A  = 0 keine Normierung der Koordinaten (oberes Nibble)
��             1       Normierung der Koordinaten (oberes Nibble)
��             Farbe (unteres Nibble)
��             0 = schwarz
��             1 = wei~
��        B  = Nummer der anzusprechenden Graphikseite (0 oder 1)
��        HL = Adress� eine� ach� Byte� lange� Versorgungsblocke� �
���������������mit folgendem Aufbau:
���������������x1 = x-Koordinate des Startpunktes (0 - 639)
���������������y� � y-Koordinat� de� Startpunkte�
���������������     (0 - 449 normiert, 0 - NNN nicht normiert)
���������������x2 = x-Koordinate des Endpunktes   (0 - 639)
���������������y2 = y-Koordinate des Endpunktes
���������������     (0 - 449 normiert, 0 - NNN nicht normiert)


��Funktion 30: Kreis zeichnen

��Import: A  = Farbe
��             0 = schwarz
��             1 = wei~
��        B  = Nummer der anzusprechenden Graphikseite (0 oder 1)
��        HL = Adress� eine� sech� Byte� lange� Versorgungsblok�
���������������ke� mi� folgende� Aufbau:
���������������xm = x-Koordinate des Mittelpunktes (0 - 639)
���������������ym = y-Koordinate des Mittelpunktes (0 - 449)
���������������r  = Radius                         (>= 0   )    

��Hinweis: Die Ausgabe erfolgt grunds{tzlich normiert!
.PA���Funktion 31: Kreisbogen zeichnen

��Import: A  = Farbe
��             0 = schwarz
��             1 = wei~
��        B  = Nummer der anzusprechenden Graphikseite (0 oder 1)
��        HL = Adress� eine� zw|lf Byte� lange� �
���������������Versorgungsblockes mit folgendem Aufbau:
���������������xm = x-Koordinate des Mittelpunktes (0 - 639)
���������������ym = y-Koordinate des Mittelpunktes (0 - 449)
���������������x1 = x-Koordinate des Startpunktes  (0 - 639)
���������������y1 = y-Koordinate des Startpunktes  (0 - 449)
���������������x2 = x-Koordinate des Endpunktes    (0 - 639)
���������������y2 = y-Koordinate des Endpunktes    (0 - 449)      

��Hinweis: Die Ausgabe erfolgt grunds{tzlich normiert!
�����������De� Kreisboge� wir� vo� Start- zu� Endpunk�� entgegen�
�����������gesetz� de� Uhrzeigersin� gezeichnet.
.PA���Funktion 33: Rechteckigen Bereich kopieren

��Import� �  �  � Quellrechtec� i� Graphikseit� �       (obere� Nibble)
��              � Quellrechtec� i� Graphikseit� �       (obere� Nibble)
��             1� Quellrechtec� i� Buffe� de� Aufrufer� (obere� Nibble)
��              � Zielrechtec�  i� Graphikseit� �       (obere� Nibble)
��              � Zielrechtec�  i� Graphikseit� �       (obere� Nibble)
��             1� Zielrechtec�  i� Buffe� de� Aufrufer� (obere� Nibble)
��                (Quell- und�� Zielrechtec� i� Buffe� de� Aufru�
������������������fer� is� nich� erlaubt)
����������DE = Adress� de� Buffer� de� Aufrufer� (entf{ll�� be� �
���������������Kopiere� vo� eine� Graphikseit� i� di� ander� bzw� �
���������������au� de� Bildschirm)
          HL = Adress� eine� ach� bi� zw|l� Byte� lange� Versor�
���������������gungsblocke� mi� folgende� Aufbau:
���������������x1 = x-Koordinat� der� untere� linke� Eck� de�      �
��������������������Quellrechteck� (� - 639�� entf{ll� be� Kopie�
��������������������re� au� Buffe� de� Aufrufers)
���������������y1 = y-Koordinat� der� untere� linke� Eck� de�      �
��������������������Quellrechteck� (� - NNN�� entf{ll� be� Kopie�
��������������������re� au� Buffe� de� Aufrufers)
���������������x2 = x-Koordinat� der� untere� linke� Eck� de�      �
��������������������Zielrechteck� (� - NNN� entf{ll� be� Kopiere� �
��������������������i� Buffe� de� Aufrufers)
���������������y2 = y-Koordinat� der� untere� linke� Eck� de�      �
��������������������Zielrechteck� (� - NNN� entf{ll� be� Kopiere� �
��������������������i� Buffe� de� Aufrufers)
���������������dx = horizontal� Kantenl{ng� de� z�� kopierende� �
��������������������Rechteck� (� - 640)
���������������dy = vertikal� Kantenl{ng� de� z��� kopierende� �
��������������������Rechteck� (� - NNN+1)
.PA�5   Festplatte

5.1 Generieren eines Festplattensystems

����U� da� Festplatte� CP/� Syste� au� Ihre� Geni� III� einsetze� �
����z�� k|nnen�� m}sse� Si� vorhe� ei� Geni� III� Flopp�� Dis� �
����CP/Mplu� Syste� Versio� 3b erworbe� haben�� Vergewisser� Si� �
����sich� da� di� Seriennumme� de� Festplattensystem� mi� de� de� �
����Flopp�� Dis� System� }bereinstimmt�� Nun gehe� Si� gena�� i� �
����nachstehende� Reihenfolg� vor:

����a� Lade� Si� da� Flopp� Dis� CP/� Syste� ur.
����b) Fertige� Si� jeweil� mindesten� zwei� Sicherungskopie� de� �
�������beide� gelieferte� Systemdiskette� a� (wi� i� Kapite� 1.� �
�������beschrieben).
����c) Lege� Si� di� Initialisierungsdiskett� f}� da� Festplat�
�������tensyste�� i� Flopp� Laufwer� � ei� un� lade� Si� erneu� �
�������ur.

����Di� i� Geni� III� eingebaut� bzw�� a� de� Geni� III� exter� �
����angeschlossen� Festplatt� wir� nu� formatiert�� Anschlie~en� �
����werde� automatisc� di� beide� Betriebssystemdateie� CPM3.SY� �
����un� CCP.CO� au� de� Festplatt� angelegt� Sollt� di� Festplat�
����t� bereit� formatier� sein�� erschein� ein� Sicherheitsabfra�
����ge� u� ei� unbeabsichtigte� L|sche� de� Festplatt� z� verhin�
����dern� Weiterhi� wir� ein� Tabell� au� de� Bildschir� ausgege�
����ben�� di� di� Laufwerkseinteilun� un� di� Kapazit{� de� ein�
����zelne� Laufwerk� angibt.

����Di� Initialisierungsdiskett� ben|tige� Si� jetz� nich� mehr� �
����au~e� ei� "Hea� Crash� w}rd� di� Formatierun� Ihre� Festplat�
����t� zerst|ren�� I� diese� Fall� m}~te� Si� di� obe� angegeben� �
����Generierungssequen� erneu� durchf}hren.

����Hinweis:� Da� Dienstprogram� FORMA� unterst}tz� jetz� auc� di� �
����Wechselplatte�� fall� ein� Fest-/Wechselplattenstatio� a� Ih�
����re� Geni� III� angeschlosse� ist.
.PA�5.2 Das Dienstprogramm BACKUP:

����Aufruf:

����    BACKUP (Modus) (Quellaufwerk) (Ziellaufwerk) ({V})

����F}r "Modus" kann angegeben werden:
����S = Sichern      einer Disk
����R = Restaurieren einer Disk

����F}� "Quell-�� bzw�� "Ziellaufwerk� m}sse� Si� di� Name� de� �
����betreffende� logische� "CP/M"-Laufwerk� angeben� Wir� di� Op�
����tio� "{V}� angegeben�� wir� jede� kopiert� Bloc� verifiziert� �
����Bi� au� "{V}� werde� all� nich� angegebene� Paramete� vo� �
����BACKU� abgefragt� 

����Bei�� Sicher� eine� Dis� forder� BACKU� automatisc� soviel� �
����(scho� formatierte� Zieldiskette� an� wi� ben|tig� werden� u� �
����de� Inhal� de� Quellaufwerk� z� sichern.

����Bei� Restauriere� eine� Dis� forder� BACKU� automatisc� all� �
����bei� Sicher� beschriebene� Quelldiskette� an� Durc� eine� in�
����telligente� Sicherungsalgorithmu� is� ei� Verwechsel� de� Si�
����cherungsdiskette� untereinande� un� mi�� andere� Diskette� �
����ausgeschlossen.

����Achtung: BACKUP kopiert nur die belegten Bl|cke einer Disk!

����Beispiel:

����                BACKUP S B D {V}

����All� belegte� Bl|ck� de� Dis� � werde� au� ein� ode� mehrer� �
����Diskette� � kopier� un� verifiziert.
.PA�6 ��Einschr{nkungen

6.1 RST 38H

����Di� Treiberprogramm� de� Geni� III� arbeite� interruptgesteu�
����ert�� u� ein� Softwareuh� un� gepuffert� Tastatureingab� zu� �
����Verf}gun� stelle� z� k|nnen�� D� de� GenieIII� aufgrun� sei�
����ne� Hardwar� Architektu� nu� i� Interruptmodu� �� arbeite� �
����kann�� sin� all� CP/� Programme�� di� de� Restar� Vekto� 3� �
����benutzen� au� de� Geni� III� nich� lauff{hig� Hie� ein� List� �
����diese� Programme:

����- SI�  Debugge� (Digita� Research)
����- ZSI� Debugge� (Digita� Research)
����- Mi-� Compile� (G.Kersting/H.Rose)

����Die� bedeute� jedoc� kein� echt� Einschr{nkung�� d� de� vo� �
����Digita� Researc� mitgeliefert� Debugge� SI� sowies� nu� 808� �
����Opcodes� nich� jedoc� Z8� Opcode� verarbeite� kann� Stat� de� �
����Debugger� ZSI� l{~� sic� de� Debugge� Trace-8� de� Firm� Lau�
����terbac� Datentechni� einsetzen� be� de� de� vo� Debugge� ver�
����wendet� Restar� Vekto� fre� w{hlba� ist�� Vo� Mi-à Compile� �
����is� ein� Spezialversio� erh{ltlich� di� de� Restar� Vekto� 3� �
����benutzt.


6.2 Inkompatibilit{ten zu CP/M Version 2.2

����Grunds{tzlic� is� CP/Mplu� Versio� � vol� kompatibel z� CP/� �
����Versio� 2.2�� wen� di� unte� diese� Systeme� arbeitende� Pro�
����gramm� Betriebssystemfunktione� nu� }be� di� BDOӠ Schnitt�
����stell� aufrufen�� Be� zeichenorientierte� Ein-/Ausgab� sin� �
����auc� noc� di� BIO� Schnittstelle� zueinande� kompatibel.

����E� existiere� jedoc� einig� wenig� CP/� 2.�� Programme�� di� �
����dies� offizielle� Betriebssystemschnittstelle� umgehe� un� �
����dami�� unte� de� Syste� CP/Mplu� nich� ode� nu� beschr{nk� �
.PA�����lauff{hi� sind�� Hie� ein� List� de� bi� dat� bekannte� Pro�
����gramme:

����- MicroShell Kommandoprozessor (New Generation Systems)
����- DPATCH     Disk Editor       (Advanced Micro Techniques)
    - Trace-80   Debugger          (Lauterbach Datentechnik)

����Abhilfema~nahmen:
����De� Kommandoprozesso� MicroShel� sollt� nich� meh� eingesetz� �
����werden�� d� de� gr|~t� Tei� seine� Funktione� unte� CP/Mplu� �
����sowies� z� Verf}gun� steht.

����U� DPATC� unte� CP/Mplu� vol� lauff{hi� z� machen� m}sse� Si� �
����di� au� de� Systemdiskett� mitgeliefert� "Residen�� Syste� �
����Extension� BIOS22.RS� mi� de� Dienstprogram� GENCO� a� DPATC� �
����anbinden� Aufruf:
����                GENCOM DPATCH BIOS22
����D� i� CP/� Versio� 2.� nu� ein� maximal� Laufwerkskapazit{� �
����vo� � MByte� unterst}tz� wird�� l{~� auc� DPATC� bei� physi�
����sche� Diskzugrif� (Funktio� 5� kein� gr|~er� Laufwerkskapazi�
����t{� zu.

����Trace-8�� ben|tig� eine� kleine� Patch�� u� i� CP/Mplu� ord�
����nungsgem{� BDO� Aufruf� durchf}hre� z� k|nnen�� Diese� Patc� �
����kan� mi� Trace-8� selbs� vorgenomme� werden�� Aufrufsequen� �
����f}� Versio� 2.07:
����                T80 T80
����                  A 3DFF   NOP
����                           NOP
����                  SAVE T80 100 50FF

6.3 Lieferumfang

����Da� Dienstprogram� COPYSY� vo� Digita� Researc� wir� au� ur�
����heberrechtliche Gr}nde nich�� mi�� CP/Mplu� f}� TC� �
����GenieIII� ausgeliefert� Allerding� stell� da� Dienstprogram� �
����KOPIEҠ mi�� de� Optio� SYSTE� eine� vollwertige� Ersat�� f}� �
����COPYSY� dar.�7 Unterst}tzung des Benutzers

��Weiter� Unterst}tzun� (wi� Bearbeitun� vo� Fehler� ode� de� Be�
��zu� vo� Erg{nzungen� wir� nu� gew{hrt�� wen� Si� untenstehend� �
��Mitteilun� a� di� folgend� Adress� senden:

����                Thomas Holte
����                Sommerstr.16
����           8504 Stein


��Ich/Wir habe(n) das Betriebssystem CP/M 3 bezogen.


��Absender: .....................................................

��          .....................................................

��          .....................................................


��Versions- un� Seriennumme� Ihre� Systems� .....................


��Bezogen am: .............. bei: ...............................

��                                ...............................

��                                ...............................


��Unterschrift: .................................................
.PA�.OP
.HEGenie IIIs CP/M Version 3a


















                 A N L A G E   A :   S Y S T A B
.PA�.OP


















       A N L A G E   B :   D I S K E T T E N F O R M A T E
.PA�Osborne I (einfache Dichte):

                 - einseitig
                 - einfache Dichte
                 - 40 nutzbare Spuren
                 - 10 Sektoren pro Spur
                 - 256 Bytes Sektorl{nge
                 - Nummer des ersten Sektors: 1
                 - 2 Steps von Spur zu Spur
                 - "Interleaving"-Faktor: 1
                 - 2 KBytes Blockgr|~e
                 - 2 KBytes "Directory"-Gr|~e
                 - ]bersetzungsfaktor: 2
                 - 3 Systemspuren


Osborne I (doppelte Dichte):

                 - einseitig
                 - doppelte Dichte
                 - 40 nutzbare Spuren
                 - 5 Sektoren pro Spur
                 - 1024 Bytes Sektorl{nge
                 - Nummer des ersten Sektors: 1
                 - 2 Steps von Spur zu Spur
                 - "Interleaving"-Faktor: 1
                 - 1 KByte Blockgr|~e
                 - 2 KBytes "Directory"-Gr|~e
                 - ]bersetzungsfaktor: 1
                 - 3 Systemspuren
.PA�Tandy TRS-80 Modell 4:

                 - einseitig
                 - doppelte Dichte
                 - einfache Dichte der ersten Spur
                 - 39 nutzbare Spuren
                 - 8 Sektoren pro Spur
                 - 512 Bytes Sektorl{nge
                 - Nummer des ersten Sektors: 1
                 - 2 Steps von Spur zu Spur
                 - "Interleaving"-Faktor: 4
                 - 1 KByte Blockgr|~e
                 - 2 KBytes "Directory"-Gr|~e
                 - ]bersetzungsfaktor: 1
                 - 0 Systemspuren


Tandy TRS-80 Modell II/12:

                 - 8-Zoll Diskettengr|~e
                 - einseitig
                 - doppelte Dichte
                 - einfache Dichte der ersten Spur
                 - 76 nutzbare Spuren
                 - 16 Sektoren pro Spur
                 - 512 Bytes Sektorl{nge
                 - Nummer des ersten Sektors: 1
                 - "Interleaving"-Faktor: 4
                 - 2 KBytes Blockgr|~e
                 - 4 KBytes "Directory"-Gr|~e
                 - ]bersetzungsfaktor: 1
                 - 1 Systemspur
.PA�IBM Personal Computer (einseitig):

                 - einseitig
                 - doppelte Dichte
                 - 40 nutzbare Spuren
                 - 8 Sektoren pro Spur
                 - 512 Bytes Sektorl{nge
                 - Nummer des ersten Sektors: 1
                 - 2 Steps von Spur zu Spur
                 - "Interleaving"-Faktor: 1
                 - 1 KByte Blockgr|~e
                 - 2 KBytes "Directory"-Gr|~e
                 - ]bersetzungsfaktor: 1
                 - 1 Systemspur
.PA�.OP


















    A N L A G E   C :   B I L D S C H I R M P A R A M E T E R
.PA�I� folgende� Tabell� finde� Si� di� empfohlene� Standardwert� f}� �
di� erste� zw|l� Registe� de� Bildschirmcontroller� MC684� i� he�
xadezimale� Schreibweis� f}� verschieden� Monitore� Dies� Tabell� �
wir� laufen� erg{nzt:

Sakata SG-1000:

6E  50  56  0A  1C  04  19  19  00  0A  6A  0A


IBM PC Monochrome Display:

60  50  50  0F  1D  09  19  1A  01  0B  6B  0B
